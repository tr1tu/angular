var gulp = require('gulp')
var concat = require('gulp-concat')
var uglify = require('gulp-uglify')

gulp.task('js', function () {
  gulp.src(['app/**/module.js', 'app/controllers/*.js'])
    .pipe(concat('app.js'))
    .pipe(gulp.dest('.'))
})