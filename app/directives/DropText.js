directives.directive('dropText', ['$compile', function($compile) {

	return {
		restrict : 'A',
		scope : {
			dropTextPrefix : '@',
			dropTextSuffix : '@',
			ngModel : '='
		},
		controller : ['$scope', function($scope) {
			$scope.onDropComplete = function(data, event) {

				var prefix = $scope.dropTextPrefix || "";
				var suffix = $scope.dropTextSuffix || "";

				if($scope.ngModel)
				{
					if($scope.ngModel[$scope.ngModel.length - 1] != " ")
						$scope.ngModel += " ";
					$scope.ngModel += prefix + data + suffix;
				}
				else
				{
					$scope.ngModel = prefix + data + suffix;
				}
			}
		}],
		compile : function compile(element, attrs) {

			element.attr('ng-drop', true);
			element.attr('ng-drop-success', 'onDropComplete($data,$event)');
			element.removeAttr('drop-text');


			return {
				pre : function preLink(scope, iElement, iAttrs, controller) {   },
				post : function postLink(scope, iElement, iAttrs, controller) {  
					$compile(iElement)(scope);
				}
			}
		}

	};

}]);