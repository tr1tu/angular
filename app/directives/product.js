directives.directive('product', [function() {

	return  {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/product.html',
		scope : {
			productData : '=',
			productDetailsFn : '&',
			addToCartFn : '&'
		},
		link : function(scope, elem, attrs) {
			scope.productDetails = function() {
				scope.productDetailsFn()(scope.productData);
			}

			scope.addToCart = function(id) {
				scope.addToCartFn()(id, 1);
			}
		}
	};

}]);