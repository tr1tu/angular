directives.directive('opinion', ['uibDateParser', '$filter', function(uibDateParser, $filter) {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/opinion.html',
		scope : {
			data : '=',
			editFn: '&?',
			loggedUser: '=',
			options : '=',
			upvoteFn : '&?',
			downvoteFn : '&?'
		},
		link : function(scope, elem, attrs) {
			scope.date = new Date(scope.data.created_at);


			scope.editOpinion = function() {
				if(scope.editFn)
					scope.editFn()(scope.data);
			}

			scope.upvote = function() {
				if(scope.upvoteFn)
				{
					scope.upvoteFn()(scope.data);
				}
			}

			scope.downvote = function() {
				if(scope.downvoteFn)
				{
					scope.downvoteFn()(scope.data);
				}
			}

			scope.checkIfUserVoted = function(userId) {
				var found = $filter('filter')(scope.data.voters, {id: parseInt(userId)}, true);

				if(found.length)
				{
					scope.userVoted = found[0].pivot.value;
				}
				else
				{
					scope.userVoted = false;
				}
			}

			scope.checkIfUserVoted(scope.loggedUser.id);
			
			scope.$watch('data.voters', function(newValue, oldValue) {
				if(newValue != oldValue)
				{
					scope.checkIfUserVoted(scope.loggedUser.id);
				}

			})


		}

	}

}]);