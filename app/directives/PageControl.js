directives.directive('pageControl', ['$timeout', function($timeout) {

	return {
		restrict : 'E',
		replace : true,
		scope : {
			options : '='
		},
		templateUrl : 'app/directives/pagecontrol.html',
		controller : ['$scope', function($scope) {

			$scope.nearbyPages = [];
			$scope.allPages = [];


			var getNearbyPages = function() {

				$scope.nearbyPages = [];

				var start;
				var end;

				if($scope.options.currentPage() - 5 > 0)
					start = $scope.options.currentPage() - 5;
				else
					start = 1;

				if(start + 9 <= $scope.options.totalPages())
					end = start + 9;
				else
					end = $scope.options.totalPages();

				for(var i = start; i <= end; i++)
					$scope.nearbyPages.push(i);

			}

			var getAllPages = function() {
				$scope.allPages = [];

				for(var i = 1; i <= $scope.options.totalPages(); i++)
					$scope.allPages.push(i);
			}
			
			$scope.$watch(function() { return $scope.options.currentPage(); }, function() { getNearbyPages(); $scope.pageSelect = $scope.options.currentPage(); });
			$scope.$watch(function() { return $scope.options.totalPages(); }, function() { getAllPages(); getNearbyPages(); });

		}]

	};


}]);