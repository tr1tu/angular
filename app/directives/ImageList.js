directives.directive('imagelist', ['$filter', function($filter) {

	return {
		restrict : 'E',
		templateUrl : 'app/directives/imagelist.html',
		scope : {
			list : '=',
			selected : '=',
			currentImage : '=',
			selectClass : '@',
			selectStyle : '@',
			imgClass : '@',
			imgStyle : '@',
			preview: '@'
		},

		replace : true,

		controller : ['$scope',function($scope) {

			$scope.imageSelect = [];

			$scope.$watch('imageSelect', function(value) {

				$scope.selected = [];
				$scope.currentImage = null;

				angular.forEach($scope.imageSelect, function(v,k) {

					var found = $filter('filter')($scope.list, { id : parseInt(v) }, true);

					if(found.length)
						$scope.selected.push(found[0]);

					$scope.currentImage = found[0];

				});

				
			});

			$scope.swapUp = function() {
				if($scope.imageSelect.length != 1)
					return;
				if(parseInt($scope.imageSelect[0]) == $scope.list[0].id)
					return;

				var found = $filter('filter')($scope.list, { id : parseInt($scope.imageSelect[0]) }, true);
				var index = $scope.list.indexOf(found[0]);
				var temp = $scope.list[index - 1];
				$scope.list[index -1] = $scope.list[index];
				$scope.list[index] = temp;



			}

			$scope.swapDown = function() {
				if($scope.imageSelect.length != 1)
					return;
				if(parseInt($scope.imageSelect[0]) == $scope.list[$scope.list.length - 1].id)
					return;

				var found = $filter('filter')($scope.list, { id : parseInt($scope.imageSelect[0]) }, true);
				var index = $scope.list.indexOf(found[0]);
				var temp = $scope.list[index + 1];
				$scope.list[index + 1] = $scope.list[index];
				$scope.list[index] = temp;



			}

		}]

	};


}]);