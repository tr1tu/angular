directives.directive('categoryfilter', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/categoryfilter.html',
		scope : {
			options : '='
		},

		controller : ['$scope', function($scope) {

			$scope.tree_data = [];

			$scope.$watch('options.categories', function(categories) {
				
				$scope.tree_data = [];

				$scope.tree_data.push({ label : $scope.options.allProductsText, data : null});

				if(categories != null)
				{
					angular.forEach(categories, function(v,k) {
						
						node = { label : v.name, children : [], data : v };
						angular.forEach(v.children, function(a,b) {
							node.children.push({ label : a.name, data : a });
						});
						$scope.tree_data.push(node);
							
					});
				}
			});

			/*var addNode = function(tree, node) {

				newnode = { label : node.name, data : node };
				children = {};

				angular.forEach(node.children, function(a,b) {
					addNode(children, a);
				});

			}*/


			$scope.tree_handler = function(branch) {
				$scope.options.setCategory(branch.data);
			}
		}]

	};

}]);