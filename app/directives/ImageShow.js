directives.directive('imageShow', [function() {

	return {

		restrict : 'E',
		replace: true,
		templateUrl : 'app/directives/imageshow.html',
		scope : {
			'data' : '='
		},
		link : function(scope, elem, attrs) {

			scope.$watch('data', function(value) {
				if(scope.data) scope.main = scope.data[0];
			});

			
			scope.setMain = function(image) {
				scope.main = image;
			}
		}

	}

}]);