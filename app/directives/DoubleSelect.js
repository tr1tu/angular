directives.directive('doubleselect', ['$filter', '$timeout', function($filter, $timeout) {	

	return {
		restrict : 'E',
		scope : {
			selectedItems : '=',
			items : '='
		},
		replace : true,
		templateUrl : 'app/directives/doubleselect.html',

		link : function(scope, element, attrs) {

			var getNotSelected = function() {
				
				angular.forEach(scope.items, function(value,key) {
					var found = false;

					angular.forEach(scope.selectedItems, function(v,k) {
						if(value.id == v.id)
							found = true;
					})

					if(!found)
					{
						scope.newItems.push(value);
					}


				});

			}

			scope.$watchGroup(['items','selectedItems'], function(value) {
				scope.newItems = [];
				getNotSelected();
			});


			scope.select = function() {
				angular.forEach(scope.all, function(value, key) {

		    		 var found = $filter('filter')(scope.newItems, {id: parseInt(value)}, true);
				     if (found.length) {
				         scope.selectedItems.push(found[0]);
				         scope.newItems.splice(scope.newItems.indexOf(found[0]), 1);
				     }
		   		
		   		});

			}

			scope.deselect = function() {

				angular.forEach(scope.selected, function(value, key) {
	    		var found = $filter('filter')(scope.selectedItems, {id: parseInt(value)}, true);
			     if (found.length) {
			         scope.newItems.push(found[0]);
			         scope.selectedItems.splice(scope.selectedItems.indexOf(found[0]), 1);
			     }
		 	});

			}

			scope.selectAll = function() {
				scope.selectedItems = scope.selectedItems.concat(scope.newItems);
    			scope.newItems.length = 0; //Clear without triggering watcher

			}

			scope.deselectAll = function() {
				scope.newItems = scope.newItems.concat(scope.selectedItems);
    			scope.selectedItems.length = 0; //Clear without triggering watcher

			}
		

		}



	};

}]);