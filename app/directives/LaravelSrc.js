directives.directive('laravelSrc', ['FileService', function(FileService) {

	return {
		restrict : 'A',
		link: function(scope, element, attrs) {
			attrs.$observe('laravelSrc', function() {
				if(attrs.laravelSrc.indexOf('http') == -1)
					attrs.$set('src', FileService.getUrlPublic(attrs.laravelSrc));
				else
					attrs.$set('src', attrs.laravelSrc);
			});

	   }


	};

}]);