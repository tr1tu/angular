directives.directive('status', ['MessagesService', function(MS) {	

	return {
		restrict : 'E',
		scope : {},
		templateUrl : 'app/directives/status.html',

		link: function($scope) {
			$scope.status = MS.status;
			$scope.statusError = MS.statusError;
		}
	}

}]);