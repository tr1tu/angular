directives.directive('cartRow', ['CartService', '$timeout', function(CartService, $timeout) {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/cartrow.html',
		scope : {
			data : '=',
			detailsFn: '&'
		},
		link : function(scope, elem, attrs) {	
			
			scope.increase = function() {
					
				CartService.addProduct(scope.data.id, scope.data.pivot.ammount*1 + 1);
			}

			scope.decrease = function() {
				
				var ammount = scope.data.pivot.ammount*1 > 1 ? scope.data.pivot.ammount*1 - 1 : 1;
				CartService.addProduct(scope.data.id, ammount);
			}

			scope.delete = function() {
				
				CartService.deleteProduct(scope.data.id);
			}

			scope.save = function() {
				CartService.addProduct(scope.data.id, scope.data.pivot.ammount*1);
			}


			scope.details = function() {
				scope.detailsFn()(scope.data);
			}

		},

		controller: ['$scope', function($scope) { 
			var timer;
			$scope.$watch(function() { return $scope.data.pivot.ammount; }, function(newValue, oldValue) {
				if(newValue == oldValue)
					return;

				$timeout.cancel(timer);
				timer = $timeout(function() { $scope.save(); }, 1000);

			});
		}]

	}

}]);