directives.directive('searchkeywords', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/searchkeywords.html',
		scope : {
			options : '='
		},
		controller : ['$scope', function($scope) {

			$scope.search = function(text) {
				$scope.options.searchFn(text);
			}

			$scope.searchKeywords = function(text) {
				var splitted = text.split(' ');

				if(splitted.length && splitted[0] != "")
					$scope.options.searchFn(text.split(' '));
				else
					$scope.options.searchFn([]);
			}

			$scope.clear = function() {
				$scope.searchText = "";
				$scope.search("");
			}

		}]

	};

}]);