//Requires angular-strap

directives.directive('sizingList', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/sizinglist.html',
		scope : {
			data : '=',
			selectedSizing : '=',
			selectedSize : '=',
			measureUnit : '@',
			sizingPlaceholder : '@'
		},
		link : function(scope, elem, attrs) {

			scope.$watch('data', function(newValue, oldValue) {
				if(newValue != oldValue)
				{
					if(scope.data && scope.data.length >= 1)
					{
						if('selectedSizing' in attrs)
						{
							scope.selectedSizing = scope.data[0];	
						}
						
					}
				}
			});

			scope.selectSize = function(size) {
				if('selectedSize' in attrs)
				{
					scope.selectedSize = size;
				}
			}


		}

	}

}]);