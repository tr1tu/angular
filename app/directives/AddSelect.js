directives.directive('addSelect', ['$filter', function ($filter) {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/directives/addselect.html',
        scope: {
            data: '=',
            selected: '=',
            withOrder: '='
        },

        link: function (scope, elem, attrs) {

            scope.add = function (item) {

                if (!item || item == "")
                    return;

                var found = false;
                angular.forEach(scope.data, function (v, k) {
                    if (v == item)
                        found = true;
                });
                if (!found) {
                    scope.data.push(item);
                    scope.newItem = "";
                }

            }

            scope.delete = function () {
                angular.forEach(scope.selected, function (value, key) {
                    var found = $filter('filter')(scope.data, value, true);
                    if (found.length) {
                        scope.data.splice(scope.data.indexOf(found[0]), 1);
                    }
                });

            }

            scope.moveUp = function () {
                for (var i = 1; i <= scope.data.length - 1; i++) {
                    if (scope.selected.indexOf(scope.data[i]) >= 0) {
                        var temp = scope.data[i - 1];
                        scope.data[i - 1] = scope.data[i];
                        scope.data[i] = temp;
                    }
                }


            }

            scope.moveDown = function () {

                for (var i = scope.data.length - 2; i >= 0; i--) {
                    if (scope.selected.indexOf(scope.data[i]) >= 0) {
                        var temp = scope.data[i + 1];
                        scope.data[i + 1] = scope.data[i];
                        scope.data[i] = temp;
                    }
                }

            }

        }

    }

}]);