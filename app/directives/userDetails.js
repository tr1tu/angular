directives.directive('userDetails', [function () {

    return {
        restrict: 'E',
        replace: true,
        templateUrl: 'app/directives/userdetails.html',
        scope: {
            data: '=',
            title: '@',
            link: '@'
        }

    }

}]);