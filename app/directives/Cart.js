directives.directive('cart', ['CartService', function(CartService) {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/cart.html',
		scope : {
			detailsFn: '&',
			totalText : '@?',
			emptyText : '@?',
		},
		link : function(scope, elem, attrs) {	

			scope.cart = CartService;

			scope.details = function(product) {
				scope.detailsFn()(product);
			}

			if(!scope.totalText)
				scope.totalText = "TOTAL";

			if(!scope.emptyText)
				scope.emptyText = "Your shopping cart is empty.";

		}

	}

}]);