//Requires ui bootstrap

directives.directive('colorList', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/colorlist.html',
		scope : {
			data : '=',
			selected : '='
		},
		link : function(scope, elem, attrs) {

			scope.select = function(color) {
				if('selected' in attrs)
					scope.selected = color;
			}

		}


	}

}]);