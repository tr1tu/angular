directives.directive('tagfilter', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/tagfilter.html',
		scope : {
			tags : '=',
			selected : '='
		}

	};

}]);