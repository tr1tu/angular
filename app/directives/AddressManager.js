directives.directive('addressManager', ['UserService', '$timeout', function (UserService, $timeout) {

	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/directives/addressmanager.html',
		scope: {
			data: '=',
			selected: '=',
			translations: '=?'
		},

		link: function (scope, elem, attrs) {

			if (!scope.translations)
				scope.translations = {};

			if (!scope.translations || !scope.translations.edit) scope.translations.edit = 'Edit';
			if (!scope.translations || !scope.translations.cancel) scope.translations.cancel = 'Cancel';
			if (!scope.translations || !scope.translations.save) scope.translations.save = 'Save';
			if (!scope.translations || !scope.translations.add) scope.translations.add = 'Add new address';
			if (!scope.translations || !scope.translations.name) scope.translations.name = 'Name';
			if (!scope.translations || !scope.translations.lastname) scope.translations.lastname = 'Last name';
			if (!scope.translations || !scope.translations.dnicif) scope.translations.dnicif = 'DNI/CIF';
			if (!scope.translations || !scope.translations.country) scope.translations.country = 'Country';
			if (!scope.translations || !scope.translations.province) scope.translations.province = 'Province';
			if (!scope.translations || !scope.translations.city) scope.translations.city = 'City';
			if (!scope.translations || !scope.translations.zip_code) scope.translations.zip_code = 'ZIP code';
			if (!scope.translations || !scope.translations.address) scope.translations.address = 'Address';
			if (!scope.translations || !scope.translations.phone) scope.translations.phone = 'Phone';
			if (!scope.translations || !scope.translations.mob_phone) scope.translations.mob_phone = 'Mobile phone';
			if (!scope.translations || !scope.translations.description) scope.translations.description = 'Description';
			if (!scope.translations || !scope.translations.deleted) scope.translations.deleted = 'Deleted!';
			if (!scope.translations || !scope.translations.delete) scope.translations.delete = 'Delete selected address';

			scope.$watch('data', function () {
				scope.select();
			});

			if (!scope.data) {
				//Load addresses data
				UserService.indexSelfAddress()
					.then(
					function (result) {
						scope.data = result.data;
					}
					);

			}

			scope.add = function () {
				scope.address = {};
			}

			scope.edit = function () {
				scope.address = angular.copy(scope.selected);
			}

			scope.save = function () {


				if (scope.address.id) {
					//Update
					UserService.updateSelfAddress(scope.address)
						.then(
						function (result) {
							return UserService.indexSelfAddress();
						}
						)
						.then(
						function (result) {
							scope.data = result.data;
							scope.cancelEdit();
						}
						);
				}
				else {
					var newId;

					//Store
					UserService.storeSelfAddress(scope.address)
						.then(
						function (result) {
							newId = result.data.id;
							return UserService.indexSelfAddress();
						}
						)
						.then(
						function (result) {
							scope.data = result.data;
							scope.cancelEdit();
							scope.selectedId = newId;
						}
						);
				}
			}

			scope.cancelEdit = function () {
				scope.address = null;
			}

			scope.select = function () {
				angular.forEach(scope.data, function (value, index) {
					if (value.id == scope.selectedId) {
						scope.selected = scope.data[index];
					}
				});
			}

			scope.delete = function () {
				var index = scope.data.indexOf(scope.selectedId);
				UserService.destroySelfAddress(scope.selectedId)
					.then(
					function (result) {
						scope.data.splice(index, 1);
						scope.selected = undefined;
					},
					function (error) {
					}
					);
			}

		},

		controller: ['$scope', function ($scope) {

		}]

	}

}]);