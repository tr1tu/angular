directives.directive('invoice', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/invoice.html',
		scope : {
			order : '=',
			company : '='
		},
		link : function(scope, elem, attrs) {	
		
			scope.isInvoiceCreated = function() {
				if(!scope.order)
					return false;

				return scope.order.invoice != null;
			}

			scope.$watch('order', function(value, old) {
				if(value != old)
				{
					scope.pages = [];
					for(var i = 0; i < scope.order.items.length; i++)
					{
						var index = Math.floor(i / 8);
						if(!scope.pages[index]) scope.pages[index] = [];
						scope.pages[index].push(scope.order.items[i]);
						scope.pages[index].id = index;
					}
				}


			}, true);



		},

	}

}]);