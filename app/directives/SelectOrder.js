//Requires ng-strap

directives.directive('selectorder', [function() {

	/**
		options
			orders: select order options
			changeOrderFn: function called when selected order is changed
			placeholder: placeholder text for select
	**/


	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/selectorder.html',
		scope : {
			options : '=',
			selectedOrder : '='
		},

		controller : ['$scope', function($scope) {

			$scope.orders = [];

			if(!$scope.options.placeholder)
				$scope.options.placeholder = "Select order";

			$scope.$watch('options.orders', function(orders) {

				$scope.orders = [];

				angular.forEach(orders, function(v,k) {
					var neworder = { 
						label : v.name + (v.type == 'desc' ? '<span class="glyphicon glyphicon-sort-by-attributes-alt"></span>' : '<span class="glyphicon glyphicon-sort-by-attributes"></span>'),
						value : v 
					};

					$scope.orders.push(neworder);
				});

			});

			$scope.$watch('selectedOrder', function(item) {
				if(item)
					$scope.options.changeOrderFn(item.field, item.type);
			});


		}]

	};

}]);