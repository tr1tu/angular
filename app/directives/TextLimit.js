directives.directive('textLimit', [function() {

	return {
		restrict : 'E',
		replace : true,
		scope : {
			text : '@',
			max : '@',
			moreAction : '&?',
			moreText : '@',
			lessText : '@'

		},
		templateUrl : 'app/directives/textlimit.html',

		controller : ['$scope', function($scope) {


			$scope.$watch('text', function(val) {
				$scope.textOut = val.substring(0, $scope.max);
			});
			
			$scope.fullMoreAction = function() {
				if($scope.moreAction)
					$scope.moreAction()();
				else
					$scope.textOut = $scope.text;
			}

			$scope.lessAction = function() {
				$scope.textOut = $scope.text.substring(0, $scope.max);
			}
			

		}],

		compile : function(element, attrs) {
			
			if(!attrs.text) attrs.text = "";
			if(!attrs.max) attrs.max = 200;
			if(!attrs.moreText) attrs.moreText = "More";
			if(!attrs.lessText) attrs.lessText = "Less";
		}

	};

}]);