directives.directive('processSteps', [function() {

	return {
		restrict : 'E',
		replace : true,
		templateUrl : 'app/directives/processsteps.html',
		scope : {
			steps : '=',
			currentStep : '=',
			stepClick : '&?'
		},
		link : function(scope, elem, attrs) {	
		
			scope.click = function(step) {
				if(scope.stepClick)
					scope.stepClick()(step);
			}

			scope.isClickable = function() {
				return stepClick != null;
			}
			

		},

	}

}]);