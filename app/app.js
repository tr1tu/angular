var app = angular.module("MainApp", ['ui.router', 'satellizer', 'ui.grid', 'ui.grid.selection',
    'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.pagination', 'pascalprecht.translate', 'myDirectives', 'color.picker',
    'myFilters', 'ui.date', 'checklist-model', 'angularBootstrapNavTree', 'ngSanitize', 'mgcrea.ngStrap.select', 'ngFileUpload',
    'ngDraggable', 'ui.bootstrap', 'ngAnimate', 'ngCookies', 'angular-loading-bar', 'summernote', 'dndLists', 'mgcrea.ngStrap.navbar',
    'mgcrea.ngStrap.datepicker', 'mgcrea.ngStrap.timepicker', 'bs-validation', 'toaster', 'vcRecaptcha', 'colorpicker.module',])
    .config(['$stateProvider', '$authProvider', '$httpProvider', '$provide', '$urlRouterProvider', 'API_URL', '$translateProvider', '$locationProvider',
        function ($stateProvider, $authProvider, $httpProvider, $provide, $urlRouterProvider, API_URL, $translateProvider, $locationProvider) {


            function redirectWhenLoggedOut($q, $injector) {

                return {

                    responseError: function (rejection) {

                        // Need to use $injector.get to bring in $state or else we get
                        // a circular dependency error
                        var $state = $injector.get('$state');

                        // Instead of checking for a status code of 400 which might be used
                        // for other reasons in Laravel, we check for the specific rejection
                        // reasons to tell us if we need to redirect to the login state
                        var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid', 'user_not_found'];

                        // Loop through each rejection reason and redirect to the login
                        // state if one is encountered
                        angular.forEach(rejectionReasons, function (value, key) {

                            if (rejection.data != null && rejection.data.error === value) {

                                // If we get a rejection corresponding to one of the reasons
                                // in our array, we know we need to authenticate the user so 
                                // we can remove the current user from local storage
                                localStorage.removeItem('user');

                                // Send the user to the auth state so they can login
                                $state.go('auth');
                            }
                        });

                        return $q.reject(rejection);
                    }
                }
            }



            // Setup for the $httpInterceptor
            $provide.factory('redirectWhenLoggedOut', redirectWhenLoggedOut);

            // Push the new factory onto the $http interceptor array
            $httpProvider.interceptors.push('redirectWhenLoggedOut');

            $authProvider.loginUrl = API_URL + "/authenticate";


            var _skipIfAuthenticated = ['$q', '$state', 'AuthService', '$timeout', function ($q, $state, AuthService, $timeout) {
                var defer = $q.defer();
                if (AuthService.getUser()) {
                    $timeout(function () {
                        $state.go('home');
                    });
                    defer.reject();
                } else {
                    defer.resolve();
                }
                return defer.promise;
            }]

            var _redirectIfNotAuthenticated = ['$q', '$state', 'AuthService', '$timeout', function ($q, $state, AuthService, $timeout) {
                var defer = $q.defer();
                if (AuthService.getUser()) {
                    defer.resolve();
                } else {
                    $timeout(function () {
                        $state.go('auth');
                    });
                    defer.reject();
                }
                return defer.promise;
            }]

            var _redirectIfNotAdmin = ['$q', '$state', 'AuthService', '$timeout', function ($q, $state, AuthService, $timeout) {
                var defer = $q.defer();
                if (AuthService.userIsAdmin()) {

                    defer.resolve();
                } else {
                    AuthService.logout();
                    $timeout(function () {
                        $state.go('auth');
                    });
                    defer.reject();
                }
                return defer.promise;
            }]

            var _redirectIfPasswordExpired = ['$q', '$state', 'AuthService', '$timeout', function ($q, $state, AuthService, $timeout) {
                var defer = $q.defer();
                if (AuthService.getUser().expired_password) {
                    $timeout(function () {
                        $state.go('changepassword');
                    });
                    defer.reject();
                } else {
                    
                    defer.resolve();
                }
                return defer.promise;
            }]

            $locationProvider.html5Mode(true);


            $stateProvider
                .state('common', {
                    templateUrl: 'app/views/common.html',
                    abstract: true,
                    controller: 'CommonController'
                })
                .state('commonauth', {
                    templateUrl: 'app/views/common.html',
                    abstract: true,
                    controller: 'CommonController',
                    resolve: {
                        redirectIfNotAuthenticated: _redirectIfNotAuthenticated,
                        redirectIfPasswordExpired: _redirectIfPasswordExpired
                    }
                })
                .state('commonadmin', {
                    templateUrl: 'app/views/admin/commonadmin.html',
                    abstract: true,
                    controller: 'CommonAdminController',
                    resolve: {
                        redirectIfNotAdmin: _redirectIfNotAdmin,
                        redirectIfPasswordExpired: _redirectIfPasswordExpired
                    }
                })
                .state('admin', {
                    url: '/admin',
                    templateUrl: 'app/views/admin/home.html',
                    controller: 'HomeController',
                    parent: 'commonadmin'
                })
                .state('auth', {
                    url: '/auth',
                    templateUrl: 'app/views/user/login.html',
                    controller: 'AuthController',
                    parent: 'common',
                    resolve: {
                        skipIfAuthenticated: _skipIfAuthenticated
                    }
                })
                .state('register', {
                    url: '/register',
                    templateUrl: 'app/views/user/register.html',
                    controller: 'AuthController',
                    parent: 'common',
                    resolve: {
                        skipIfAuthenticated: _skipIfAuthenticated
                    }
                })
                .state('changepassword', {
                    url: '/changepassword',
                    templateUrl: 'app/views/user/change_password.html',
                    controller: 'ChangePasswordController',
                    parent: 'common',   //Cannot use commonauth because of redirectIfPasswordExpired
                    resolve : {
                        redirectIfNotAuthenticated: _redirectIfNotAuthenticated
                    }
                })
                .state('userlist', {
                    url: '/admin/user/list/:id?',
                    parent: 'commonadmin',
                    templateUrl: 'app/views/admin/user/list.html',
                    controller: 'UserController',
                })
                .state('usercreate', {
                    url: '/admin/user/create',
                    templateUrl: 'app/views/admin/user/create.html',
                    controller: 'UserController',
                    parent: 'commonadmin'
                })
                .state('userupdate', {
                    url: '/admin/user/update',
                    templateUrl: 'app/views/admin/user/edit.html',
                    controller: 'UserController',
                    parent: 'commonadmin'
                })
                .state('profileedit', {
                    url: '/profile/edit',
                    templateUrl: 'app/views/user/editprofile.html',
                    controller: 'ProfileController',
                    parent: 'commonauth'
                })
                .state('rolelist', {
                    url: '/admin/role/list',
                    templateUrl: 'app/views/admin/role_permission/rolelist.html',
                    controller: 'RolePermissionController',
                    parent: 'commonadmin'
                })
                .state('permissionlist', {
                    url: '/admin/permission/list',
                    templateUrl: 'app/views/admin/role_permission/permissionlist.html',
                    controller: 'PermissionController',
                    parent: 'commonadmin'
                })
                .state('productlist', {
                    url: '/admin/product/list',
                    templateUrl: 'app/views/admin/product/list.html',
                    controller: 'ProductController',
                    parent: 'commonadmin'
                })
                .state('productimport', {
                    url: '/admin/product/import',
                    templateUrl: 'app/views/admin/product/import.html',
                    controller: 'ProductImportController',
                    parent: 'commonadmin'
                })
                .state('sizinglist', {
                    url: '/admin/sizing/list',
                    templateUrl: 'app/views/admin/product/sizinglist.html',
                    controller: 'SizingController',
                    parent: 'commonadmin'
                })
                .state('taxlist', {
                    url: '/admin/tax/list',
                    templateUrl: 'app/views/admin/product/taxlist.html',
                    controller: 'TaxController',
                    parent: 'commonadmin'
                })
                .state('discountlist', {
                    url: '/admin/discount/list',
                    templateUrl: 'app/views/admin/product/discountlist.html',
                    controller: 'DiscountController',
                    parent: 'commonadmin'
                })
                .state('stocklist', {
                    url: '/admin/stock/list',
                    templateUrl: 'app/views/admin/product/stocklist.html',
                    controller: 'StockController',
                    parent: 'commonadmin'
                })
                .state('increaselist', {
                    url: '/admin/increase/list',
                    templateUrl: 'app/views/admin/product/increaselist.html',
                    controller: 'IncreaseController',
                    parent: 'commonadmin'
                })
                .state('categorylist', {
                    url: '/admin/category/list',
                    templateUrl: 'app/views/admin/product/categorylist.html',
                    controller: 'CategoryController',
                    parent: 'commonadmin'
                })
                .state('shipmentlist', {
                    url: '/admin/shipment/list',
                    templateUrl: 'app/views/admin/shipment_payment/shipmentlist.html',
                    controller: 'ShipmentController',
                    parent: 'commonadmin'
                })
                .state('paymentlist', {
                    url: '/admin/payment/list',
                    templateUrl: 'app/views/admin/shipment_payment/paymentlist.html',
                    controller: 'PaymentController',
                    parent: 'commonadmin'
                })
                .state('orderlist', {
                    url: '/admin/order/list',
                    templateUrl: 'app/views/admin/orders/list.html',
                    controller: 'OrderController',
                    parent: 'commonadmin'
                })
                .state('ticketlist', {
                    url: '/admin/ticket/list',
                    templateUrl: 'app/views/admin/ticket/list.html',
                    controller: 'TicketListController',
                    parent: 'commonadmin'
                })
                .state('ticketdetails', {
                    url: '/admin/ticket/:id',
                    templateUrl: 'app/views/ticket/details.html',
                    controller: 'TicketController',
                    parent: 'commonadmin'
                })
                .state('ticketconfig', {
                    url: '/admin/config/ticket',
                    templateUrl: 'app/views/admin/ticket/config.html',
                    controller: 'TicketConfigController',
                    parent: 'commonadmin'
                })
                .state('generalconfig', {
                    url: '/admin/generalconfig',
                    templateUrl: 'app/views/admin/config/config.html',
                    controller: 'ConfigController',
                    parent: 'commonadmin'
                })
                .state('p_productlist', {
                    url: '/product/list',
                    templateUrl: 'app/views/product/list.html',
                    controller: 'pProductController',
                    parent: 'common'
                })
                .state('p_productdetails', {
                    url: '/product/details/:id',
                    templateUrl: 'app/views/product/details.html',
                    controller: 'DetailsController',
                    parent: 'common'
                })
                .state('home', {
                    url: '/',
                    templateUrl: 'app/views/home.html',
                    controller: 'pHomeController',
                    parent: 'common',
                    title: 'Home'
                })
                .state('cart', {
                    url: '/cart',
                    templateUrl: 'app/views/orders/cart.html',
                    controller: 'CartController',
                    parent: 'common'
                })
                .state('checkout', {
                    url: '/checkout',
                    templateUrl: 'app/views/orders/checkout.html',
                    controller: 'CheckoutController',
                    parent: 'commonauth'
                })
                .state('p_orderlist', {
                    url: '/my-orders',
                    templateUrl: 'app/views/orders/orderlist.html',
                    controller: 'pOrderListController',
                    parent: 'commonauth'
                })
                .state('p_orderdetails', {
                    url: '/order/:id',
                    templateUrl: 'app/views/orders/details.html',
                    controller: 'pOrderController',
                    parent: 'commonauth'
                })
                .state('p_ticketlist', {
                    url: '/ticket/list',
                    templateUrl: 'app/views/ticket/list.html',
                    controller: 'pTicketListController',
                    parent: 'commonauth'
                })
                .state('p_ticketdetails', {
                    url: '/ticket/:id',
                    templateUrl: 'app/views/ticket/details.html',
                    controller: 'pTicketController',
                    parent: 'commonauth'
                });

            $urlRouterProvider.otherwise('/');

            //TRANSLATIONS

            $translateProvider.useStaticFilesLoader({
                files: [{
                    prefix: 'app/locale/locale-',
                    suffix: '.json'
                }]
            }).preferredLanguage('en');

            $translateProvider.useSanitizeValueStrategy('sanitize');


        }])
    .run(['$rootScope', '$state', 'i18nService', 'MessagesService', 'CartService', '$transitions', 'AuthService', '$state',
        function ($rootScope, $state, i18nService, MS, CartService, $transitions, AuthService, $state) {

            i18nService.setCurrentLang('en');

            //Get shopping cart.
            CartService.loadProducts();

            //State change
            $transitions.onStart({}, function (trans) {
                //Page title
                $rootScope.title = trans.to().title;

            });

            $state.defaultErrorHandler(function(error) {
                // This is a naive example of how to silence the default error handler.
              });

        }]);