filters.filter('griddropdown', function () {
    return function (input, map, idField, valueField, initial) {
      if(typeof map !== "undefined") {
        for (var i = 0; i < map.length; i++) {
          if(map[i][idField] == input)
          {
            return map[i][valueField];
          }
        }
      } else if(initial) {
        return initial;
      }
      return input;
    };
  });