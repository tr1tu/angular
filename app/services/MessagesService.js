app.factory('MessagesService', ['$rootScope', '$timeout', '$anchorScroll', 'toaster', '$translate', 
	function($rootScope, $timeout, $anchorScroll, toaster, $translate) { 

	return {

		addAlert : function(msg, type, timeout, removeOther) {
			removeOther = removeOther != null ? removeOther : false;

			if(removeOther)
				toaster.clear();

			var pop = {
				type : type,
				body : $translate.instant(msg, {}, null, null, null),
				bodyOutputType: 'trustedHtml'
			};

			if(timeout)
				pop.timeout = timeout;

			toaster.pop(pop);
			
		},

		clearAlerts : function() {
			toaster.clear();
		},

		getErrorsString: function(errorArray) {

			errors = "";
			angular.forEach(errorArray, function(value, key) {
				errors += value + "<br>";
			});
			return errors;

		},

		showStatus: function(text, type, time) {
			type = type || 'info';
			time = time || 5000;

			this.addAlert(text, type, time, true);
    		

    	},

	};

}]);
