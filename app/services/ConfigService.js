app.factory('ConfigService', ['$http', 'API_URL', function($http, apiUrl) {

	return {

		getCompanyData : function() {
			return $http.get(apiUrl + '/company-data');
		},

		getOrderStatus : function() {
			return $http.get(apiUrl + '/order-status');
		},

		getOrderStatusCOD : function() {
			return $http.get(apiUrl + '/order-status-cod');
		},

		getSocialMedia : function() {
			return $http.get(apiUrl + '/social-media');
		},

		getPaymentFees : function() {
			return $http.get(apiUrl + '/payment-fees');
		},

		getPaymentFinishedSteps : function() {
			return $http.get(apiUrl + '/payment-f-steps');
		},

		setMulti : function(config) {
			return $http.post(apiUrl + '/config-set', config);
		},

		index : function() {
			return $http.get(apiUrl + '/config');
		},



		///////////////////
		//slide//
		///////////////////
		
		indexSlide: function() {
		
		return $http.get(apiUrl + '/slide');
		
		},
	
		indexActiveSlide: function() {

		return $http.get(apiUrl + '/slide-active');

		},
		
		showSlide: function(id) {
		
		return $http.get(apiUrl + '/slide/' + id);
		
		},
		
		storeSlide: function(slide) {
		
		return $http.post(apiUrl + '/slide', slide);
		
		},
		
		storeMultiSlide: function(slides) {
		
		return $http.post(apiUrl + '/slide-store-multi', slides);
		
		},
		
		updateSlide: function(slide) {
		
		return $http.put(apiUrl + '/slide/' + slide.id, slide);
		
		},
		
		destroySlide: function(id) {
		
		return $http.delete(apiUrl + '/slide/' + id);
		
		},
	};

}]);