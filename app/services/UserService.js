app.factory('UserService', ['$http', 'API_URL', function ($http, apiUrl) {

	return {

		/////////
		//User//
		///////

		index: function () {

			return $http.get(apiUrl + '/user');

		},

		show: function (id) {

			return $http.get(apiUrl + '/user/' + id);

		},

		showSelf: function () {

			return $http.get(apiUrl + '/me/user');

		},

		store: function (user) {

			return $http.post(apiUrl + '/user', user);

		},

		register: function (user) {

			return $http.post(apiUrl + '/register', user);

		},

		update: function (user) {

			return $http.put(apiUrl + '/user/' + user.id, user);

		},

		updateSelf: function (user) {

			return $http.put(apiUrl + '/me/user', user);

		},

		destroy: function (id) {

			return $http.delete(apiUrl + '/user/' + id);

		},

		destroySelf: function () {

			return $http.delete(apiUrl + '/user');

		},

		//////////////////////
		//Role & Permission//
		////////////////////

		indexRole: function () {

			return $http.get(apiUrl + '/role');

		},

		showRole: function (id) {

			return $http.get(apiUrl + '/role/' + id);

		},

		storeRole: function (role) {

			return $http.post(apiUrl + '/role', role);

		},

		updateRole: function (role) {

			return $http.put(apiUrl + '/role/' + role.id, role);

		},

		destroyRole: function (id) {

			return $http.delete(apiUrl + '/role/' + id);

		},

		indexPermission: function () {

			return $http.get(apiUrl + '/permission');

		},

		showPermission: function (id) {

			return $http.get(apiUrl + '/permission/' + id);

		},

		storePermission: function (permission) {

			return $http.post(apiUrl + '/permission', permission);

		},

		updatePermission: function (permission) {

			return $http.put(apiUrl + '/permission/' + permission.id, permission);

		},

		destroyPermission: function (id) {

			return $http.delete(apiUrl + '/permission/' + id);

		},

		/////////////////////////////
		//Permission & Role Assign//
		///////////////////////////

		attachPermission: function (roleId, permId) {

			return $http.post(apiUrl + '/attach-permission', { role: roleId, perm: permId });

		},

		clearPermissions: function (roleId) {

			return $http.post(apiUrl + '/clear-permissions/' + roleId);

		},

		assignRole: function (userId, roleId) {

			return $http.post(apiUrl + '/assign-role', { user: userId, role: roleId });

		},

		clearRoles: function (id) {

			return $http.post(apiUrl + '/clear-roles/' + id);

		},


		//////////////
		//Address//
		////////////

		indexAddress: function () {

			return $http.get(apiUrl + '/address');

		},

		indexSelfAddress: function () {

			return $http.get(apiUrl + '/me/address');

		},

		showAddress: function (id) {

			return $http.get(apiUrl + '/address/' + id);

		},

		showSelfAddress: function (id) {

			return $http.get(apiUrl + '/me/address/' + id);

		},

		storeAddress: function (address) {

			return $http.post(apiUrl + '/address', address);

		},

		storeSelfAddress: function (address) {

			return $http.post(apiUrl + '/me/address', address);

		},

		updateAddress: function (address) {

			return $http.put(apiUrl + '/address/' + address.id, address);

		},

		updateSelfAddress: function (address) {

			return $http.put(apiUrl + '/me/address/' + address.id, address);

		},

		destroyAddress: function (id) {

			return $http.delete(apiUrl + '/address/' + id);

		},

		destroySelfAddress: function (id) {

			return $http.delete(apiUrl + '/me/address/' + id);

		},
	}

}]);