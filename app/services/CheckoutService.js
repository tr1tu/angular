app.factory('CheckoutService', ['OrderService', '$window', function(OrderService, $window) {

	var checkout = {
		data : {}
	};

	checkout.finish = function() {
		OrderService.storeOrder(checkout.data)
		.then(
				function(result) {
					$window.location.href = decodeURIComponent(result.data);

				},
				function(error) {
					console.log(error);
				}
			);
	}


	return checkout;

}]);