app.factory('OrderService', ['$http', 'API_URL', 'APP_URL', '$location', function($http, apiUrl, appUrl, $location) {

	return {

		//ORDER

		indexOrder : function() {
			return $http.get(apiUrl + '/order');
		},

		indexSelfOrder : function() {
			return $http.get(apiUrl + '/my-orders');
		},

		storeOrder : function(order) {
			order.success_redirect = appUrl + 'my-orders';
			order.cancel_redirect = appUrl + 'my-orders';
			order.transfer_redirect = appUrl + 'transfer-details';
			return $http.post(apiUrl + '/order', order);
		},

		showOrder : function(id) {
			return $http.get(apiUrl + '/order/' + id);
		},

		showSelfOrder : function(id) {
			return $http.get(apiUrl + '/my-order/' + id);
		},

		updateOrder : function(order) {
			return $http.put(apiUrl + '/order/' + order.id, order);
		},

		updateStatusOrder : function(orderId, status) {
			return $http.put(apiUrl + '/order-status/' + orderId, { 'status' : status });
		},

		destroyOrder : function(id) {
			return $http.delete(apiUrl + '/order/' + id);
		},

		/*fromCartOrder : function(orderId) {
			return $http.post(apiUrl + '/order-from-cart', { 'order_id' : orderId });
		},*/

		finishPaymentOrder : function(orderId) {
			var req = {};
			req.orderId = orderId;
			req.success_redirect = appUrl + 'order/' + orderId;
			req.cancel_redirect = appUrl + 'order/' + orderId;
			req.transfer_redirect = appUrl + 'transfer-details';
			return $http.post(apiUrl + '/finish-payment', req);
		},

		sendCustomerEmailOrder : function(message, userId) {
			return $http.post(apiUrl + '/order/customer-email', { 'message' : message, 'user_id' : userId });
		},

		//SHIPMENT

		indexShipment : function() {
			return $http.get(apiUrl + '/shipment');
		},

		storeShipment : function(shipment) {
			return $http.post(apiUrl + '/shipment', shipment);
		},

		showShipment : function(id) {
			return $http.get(apiUrl + '/shipment/' + id);
		},

		updateShipment : function(shipment) {
			return $http.put(apiUrl + '/shipment/' + shipment.id, shipment);
		},

		destroyShipment : function(id) {
			return $http.delete(apiUrl + '/shipment/' + id);
		},


		//PAYMENT

		indexPayment : function() {
			return $http.get(apiUrl + '/payment');
		},

		storePayment : function(payment) {
			return $http.post(apiUrl + '/payment', payment);
		},

		showPayment : function(id) {
			return $http.get(apiUrl + '/payment/' + id);
		},

		updatePayment : function(payment) {
			return $http.put(apiUrl + '/payment/' + payment.id, payment);
		},

		destroyPayment : function(id) {
			return $http.delete(apiUrl + '/payment/' + id);
		},


		//CART

		indexCart : function() {
			return $http.get(apiUrl + '/cart');
		},

		storeCart : function(cart) {
			return $http.post(apiUrl + '/cart', cart);
		},

		showCart : function(id) {
			return $http.get(apiUrl + '/cart/' + id);
		},

		updateCart : function(cart) {
			return $http.put(apiUrl + '/cart/' + cart.id, cart);
		},

		destroyCart : function(id) {
			return $http.delete(apiUrl + '/cart/' + id);
		},

		getCart : function(cartId) {
			return $http.post(apiUrl + '/cart-get', { 'cartId' : cartId });
		},

		addToCart : function(cartId, productId, ammount) {
			return $http.post(apiUrl + '/cart-add', { 'cartId' : cartId, 'productId' : productId, 'ammount' : ammount });
		},

		copyCart : function(sourceId, destId) {
			return $http.post(apiUrl + '/cart-copy', { 'sourceId' : sourceId, 'destId' : destId });
		},

	}


}]);