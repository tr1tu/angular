app.factory('ProductService', ['$http', 'API_URL', function ($http, apiUrl) {

	return {

		index: function () {

			return $http.get(apiUrl + '/product');

		},

		/*getPage : function(pageNumber, pageSize, category = null, tags = [], orderField = null, orderType = null) {

			var field = orderField ? '/' + orderField : '';
			var type = orderType ? '/' + orderType : '';

			if(!category && !(tags.length))
			{
				return $http.get(apiUrl + '/product-page/' + pageSize + field + type + '?page=' + pageNumber);
			}
			else if(category && !(tags.length))
			{
				return $http.get(apiUrl + '/product-cat-page/' + category + '/' + pageSize + field + type + '?page=' + pageNumber);
			}
			else if(!category && tags.length)
			{
				return $http.post(apiUrl + '/product-tag-page?page=' + pageNumber, { tags : tags, pageSize : pageSize, orderField : orderField, orderType : orderType });
			}
			else
			{
				return $http.post(apiUrl + '/product-cat-tag-page' + '?page=' + pageNumber, 
					{ tags : tags, categoryId : category, pageSize : pageSize, orderField : orderField, orderType : orderType });
			}

		},*/

		search: function (pageNumber, searchText, pageSize, categoryId, tags, orderField, orderType) {
			categoryId = categoryId || null;
			tags = tags || [];
			orderField = orderField || null;
			orderType = orderType || null;

			var searchOptions = {
				search_text: searchText,
				page_size: pageSize,
				order_type: orderType,
				order_field: orderField,
				tags: tags
			};

			if (categoryId != null)
				searchOptions.category_id = categoryId;

			return $http.post(apiUrl + '/product-search?page=' + pageNumber, searchOptions);
		},

		show: function (id) {

			return $http.get(apiUrl + '/product/' + id);

		},

		store: function (product) {

			return $http.post(apiUrl + '/product', product);

		},

		update: function (product) {

			return $http.put(apiUrl + '/product/' + product.id, product);

		},

		destroy: function (id) {

			return $http.delete(apiUrl + '/product/' + id);

		},

		addTags: function (productId, tags) {
			return $http.post(apiUrl + '/product-add-tag/' + productId, { tags: tags });
		},

		removeTags: function (productId, tags) {
			return $http.delete(apiUrl + '/product-remove-tag/' + productId, { tags: tags });
		},

		setTags: function (productId, tags) {
			return $http.put(apiUrl + '/product-set-tag/' + productId, { tags: tags });
		},

		withAllTags: function (tags) {
			return $http.post(apiUrl + '/product-with-all-tags', { tags: tags });
		},

		relate: function (newProduct, previousProducts) {
			return $http.post(apiUrl + '/product-relate', { new_product: newProduct, previous_products: previousProducts });
		},

		getRelated: function (productId, n) {
			return $http.get(apiUrl + '/get-related/' + productId + '/' + n);
		},

		//SIZING

		indexSizing: function () {

			return $http.get(apiUrl + '/sizing');

		},

		showSizing: function (id) {

			return $http.get(apiUrl + '/sizing/' + id);

		},

		storeSizing: function (sizing) {

			return $http.post(apiUrl + '/sizing', sizing);

		},

		updateSizing: function (sizing) {

			return $http.put(apiUrl + '/sizing/' + sizing.id, sizing);

		},

		destroySizing: function (id) {

			return $http.delete(apiUrl + '/sizing/' + id);

		},

		//TAX

		indexTax: function () {

			return $http.get(apiUrl + '/tax');

		},

		showTax: function (id) {

			return $http.get(apiUrl + '/tax/' + id);

		},

		storeTax: function (tax) {

			return $http.post(apiUrl + '/tax', tax);

		},

		updateTax: function (tax) {

			return $http.put(apiUrl + '/tax/' + tax.id, tax);

		},

		destroyTax: function (id) {

			return $http.delete(apiUrl + '/tax/' + id);

		},

		//DISCOUNT

		indexDiscount: function () {

			return $http.get(apiUrl + '/discount');

		},

		showDiscount: function (id) {

			return $http.get(apiUrl + '/discount/' + id);

		},

		storeDiscount: function (discount) {

			return $http.post(apiUrl + '/discount', discount);

		},

		updateDiscount: function (discount) {

			return $http.put(apiUrl + '/discount/' + discount.id, discount);

		},

		destroyDiscount: function (id) {

			return $http.delete(apiUrl + '/discount/' + id);

		},


		//COLOR

		indexColor: function () {

			return $http.get(apiUrl + '/color');

		},

		showColor: function (id) {

			return $http.get(apiUrl + '/color/' + id);

		},

		storeColor: function (color) {

			return $http.post(apiUrl + '/color', color);

		},

		updateColor: function (color) {

			return $http.put(apiUrl + '/color/' + color.id, color);

		},

		destroyColor: function (id) {

			return $http.delete(apiUrl + '/color/' + id);

		},


		//SIZES

		indexSize: function () {

			return $http.get(apiUrl + '/size');

		},

		showSize: function (id) {

			return $http.get(apiUrl + '/size/' + id);

		},

		storeSize: function (size) {

			return $http.post(apiUrl + '/size', size);

		},

		updateSize: function (size) {

			return $http.put(apiUrl + '/size/' + size.id, size);

		},

		destroySize: function (id) {

			return $http.delete(apiUrl + '/size/' + id);

		},

		indexImage: function () {

			return $http.get(apiUrl + '/image');

		},

		showImage: function (id) {

			return $http.get(apiUrl + '/image/' + id);

		},

		storeImage: function (image) {

			return $http.post(apiUrl + '/image', image);

		},

		updateImage: function (image) {

			return $http.put(apiUrl + '/image/' + image.id, image);

		},

		destroyImage: function (id) {

			return $http.delete(apiUrl + '/image/' + id);

		},

		clearImageOrder: function (productId) {
			return $http.delete(apiUrl + '/image-clear-order/' + productId);
		},

		//STOCK

		indexStock: function () {

			return $http.get(apiUrl + '/stock');

		},

		showStock: function (productId, sizeId) {

			return $http.get(apiUrl + '/stock/' + productId + "/" + sizeId);

		},

		storeStock: function (stock) {

			return $http.post(apiUrl + '/stock', stock);

		},

		updateStock: function (stock) {

			return $http.put(apiUrl + '/stock/' + stock.id, stock);

		},

		destroyStock: function (id) {

			return $http.delete(apiUrl + '/stock/' + id);

		},

		//INCREASE

		indexIncrease: function () {

			return $http.get(apiUrl + '/increase');

		},

		showIncrease: function (productId, sizeId) {

			return $http.get(apiUrl + '/increase/' + productId + "/" + sizeId);

		},

		storeIncrease: function (increase) {

			return $http.post(apiUrl + '/increase', increase);

		},

		updateIncrease: function (increase) {

			return $http.put(apiUrl + '/increase/' + increase.id, increase);

		},

		destroyIncrease: function (id) {

			return $http.delete(apiUrl + '/increase/' + id);

		},

		//CATEGORIES

		indexCategory: function () {

			return $http.get(apiUrl + '/category');

		},

		indexCategoryNoChildren: function () {

			return $http.get(apiUrl + '/category-no-children');

		},

		showCategory: function (id) {

			return $http.get(apiUrl + '/category/' + id);

		},

		storeCategory: function (category) {

			return $http.post(apiUrl + '/category', category);

		},

		updateCategory: function (category) {

			return $http.put(apiUrl + '/category/' + category.id, category);

		},

		destroyCategory: function (id) {

			return $http.delete(apiUrl + '/category/' + id);

		},

		addTagsCat: function (categoryId, tags) {
			return $http.post(apiUrl + '/category-add-tag/' + categoryId, { tags: tags });
		},

		removeTagsCat: function (categoryId, tags) {
			return $http.delete(apiUrl + '/category-remove-tag/' + categoryId, { tags: tags });
		},

		setTagsCat: function (categoryId, tags) {
			return $http.put(apiUrl + '/category-set-tag/' + categoryId, { tags: tags });
		},



		//OPINIONS

		indexOpinion: function () {

			return $http.get(apiUrl + '/opinion');

		},

		indexOpinionProduct: function (productId, pageNumber, pageSize, orderField, orderType) {
			pageSize = pageSize || 15;
			orderField = orderField || 'id';
			orderType = orderType || 'asc';

			if (orderField == null)
				orderField = 'id';
			if (orderType == null)
				orderType = 'asc';

			return $http.get(apiUrl + '/opinion-product/' + productId + '/' + pageSize + '/' +
				orderField + '/' + orderType + '?page=' + pageNumber);
		},

		showOpinion: function (id) {

			return $http.get(apiUrl + '/opinion/' + id);

		},

		storeOpinion: function (opinion) {

			return $http.post(apiUrl + '/opinion', opinion);

		},

		storeOpinionSelf: function (opinion) {

			return $http.post(apiUrl + '/opinion-self', opinion);

		},

		updateOpinion: function (opinion) {

			return $http.put(apiUrl + '/opinion/' + opinion.id, opinion);

		},

		updateOpinionSelf: function (opinion) {

			return $http.put(apiUrl + '/opinion-self/' + opinion.id, opinion);

		},

		destroyOpinion: function (id) {

			return $http.delete(apiUrl + '/opinion/' + id);

		},

		destroyOpinionSelf: function (id) {

			return $http.delete(apiUrl + '/opinion-self/' + id);

		},

		upvoteOpinion: function (opinionId, userId) {

			return $http.post(apiUrl + '/opinion-upvote', { opinion_id: opinionId, user_id: userId });
		},

		downvoteOpinion: function (opinionId, userId) {

			return $http.post(apiUrl + '/opinion-downvote', { opinion_id: opinionId, user_id: userId });
		},

		///////////////////
		//////IMPORT//////
		/////////////////

		import: function (products, operations, taxes, discounts, settings) {
			var params = { products: products, operations: operations, taxes: taxes, discounts: discounts };
			for (var attr in settings) {
				params[attr] = settings[attr];
			}
			return $http.post(apiUrl + '/product-import', params);
		},

		parseXml: function (filename, fields_format, subnodes) {
			return $http.post(apiUrl + '/product-parse', { filename: filename, fields_format: fields_format, subnodes: subnodes });
		},

		///////////////////
		//importprofile//
		///////////////////

		indexImportProfile: function () {

			return $http.get(apiUrl + '/importprofile');

		},

		showImportProfile: function (id) {

			return $http.get(apiUrl + '/importprofile/' + id);

		},

		storeImportProfile: function (importprofile) {

			return $http.post(apiUrl + '/importprofile', importprofile);

		},

		updateImportProfile: function (importprofile) {

			return $http.put(apiUrl + '/importprofile/' + importprofile.id, importprofile);

		},

		destroyImportProfile: function (id) {

			return $http.delete(apiUrl + '/importprofile/' + id);

		},


		///////////////////
		//operation//
		///////////////////

		indexOperation: function () {

			return $http.get(apiUrl + '/operation');

		},

		showOperation: function (id) {

			return $http.get(apiUrl + '/operation/' + id);

		},

		storeOperation: function (operation) {

			return $http.post(apiUrl + '/operation', operation);

		},

		updateOperation: function (operation) {

			return $http.put(apiUrl + '/operation/' + operation.id, operation);

		},

		destroyOperation: function (id) {

			return $http.delete(apiUrl + '/operation/' + id);

		},


		///////////////////
		//importvariable//
		///////////////////

		indexImportVariable: function () {

			return $http.get(apiUrl + '/importvariable');

		},

		showImportVariable: function (id) {

			return $http.get(apiUrl + '/importvariable/' + id);

		},

		storeImportVariable: function (importvariable) {

			return $http.post(apiUrl + '/importvariable', importvariable);

		},

		destroyImportVariable: function (id) {

			return $http.delete(apiUrl + '/importvariable/' + id);

		},

	};


}]);