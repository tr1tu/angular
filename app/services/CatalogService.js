app.factory('CatalogService', ['ProductService', '$state', '$anchorScroll', function (ProductService, $state, $anchorScroll) {

	var catalog = {
		pageSize: 18,
		data: null,
		currentPage: null,
		lastPage: 1,
		isFirst: null,
		isLast: null,
		from: null,
		to: null,
		total: null,
		selectedCategoryId: null,
		selectedCategory: null,
		selectedTags: [],
		orderField: 'name',
		orderType: 'asc',
		searchText: '',
		topAnchor: '',
		currentProduct: null,
		previousProducts: [],
		opinions: {
			pageSize: 10,
			data: null,
			currentPage: null,
			lastPage: 1,
			isFirst: null,
			isLast: null,
			from: null,
			to: null,
			total: null,
			orderField: null,
			orderType: null
		}
	};

	/*catalog.goToPage = function(page) {
		if(page >= 1 && (page <= catalog.lastPage || catalog.lastPage == 0))
		{
			ProductService.getPage(page, catalog.pageSize, catalog.selectedCategoryId, catalog.selectedTags, catalog.orderField, catalog.orderType)
			.then(
				function(result) {
					catalog.data = result.data.data;
					catalog.currentPage = result.data.current_page;
					catalog.lastPage = result.data.last_page;
					catalog.isFirst = result.data.prev_page_url == null;
					catalog.isLast = result.data.next_page_url == null;
					catalog.from = result.data.from;
					catalog.to = result.data.to;
					catalog.total = result.data.total;

				}
			);
		}
		
	}	*/

	catalog.goToPage = function (page) {
		if (page >= 1 && (page <= catalog.lastPage || catalog.lastPage == 0)) {
			ProductService.search(page, catalog.searchText, catalog.pageSize, catalog.selectedCategoryId, catalog.selectedTags, catalog.orderField, catalog.orderType)
				.then(
				function (result) {
					catalog.data = result.data.data;
					catalog.currentPage = result.data.current_page;
					catalog.lastPage = result.data.last_page;
					catalog.isFirst = result.data.prev_page_url == null;
					catalog.isLast = result.data.next_page_url == null;
					catalog.from = result.data.from;
					catalog.to = result.data.to;
					catalog.total = result.data.total;

					if (catalog.topAnchor != '') {
						$anchorScroll.yOffset = 0;
						$anchorScroll(catalog.topAnchor);
					}


				}
				);
		}
	}


	catalog.goToNext = function () {
		catalog.goToPage(catalog.currentPage + 1);
	}

	catalog.goToPrev = function () {
		catalog.goToPage(catalog.currentPage - 1);
	}

	catalog.goToFirst = function () {
		catalog.goToPage(1);
	}

	catalog.goToLast = function () {
		catalog.goToPage(catalog.lastPage);
	}

	catalog.getData = function () {
		return catalog.data;
	}

	catalog.getCurrentPage = function () {
		return catalog.currentPage;
	}

	catalog.getLastPage = function () {
		return catalog.lastPage;
	}

	catalog.setCategory = function (category) {
		catalog.selectedCategoryId = category;

		if (category != null) {
			ProductService.showCategory(category)
				.then(
				function (result) {
					catalog.selectedCategory = result.data;
				}
				);
		}
		else
			catalog.selectedCategory = null;


		//catalog.goToFirst();
	}

	catalog.setTags = function (tags) {
		catalog.selectedTags = tags;
		//catalog.goToFirst();

	}

	catalog.setOrderField = function (field) {
		catalog.orderField = field;
	}

	catalog.getOrderField = function () {
		return catalog.orderField;
	}

	catalog.setOrderType = function (type) {
		catalog.orderType = type;
	}

	catalog.getOrderType = function () {
		return catalog.orderType;
	}

	catalog.showProduct = function (product) {
		$state.go('p_productdetails', { id: product.id });
		if (catalog.previousProducts.indexOf(product.id) === -1) {
			ProductService.relate(product.id, catalog.previousProducts)
				.then(
				function (result) {

					catalog.previousProducts.push(product.id);

				}
				);
		}

	}

	if (catalog.currentPage)
		catalog.goToPage(currentPage)
	else
		catalog.goToFirst();


	catalog.opinions.goToPage = function (page) {

		if (catalog.currentProduct) {
			ProductService.indexOpinionProduct(catalog.currentProduct.id, page, catalog.opinions.pageSize,
				catalog.opinions.orderField, catalog.opinions.orderType)
				.then(
				function (result) {
					catalog.opinions.data = result.data.data;
					catalog.opinions.currentPage = result.data.current_page;
					catalog.opinions.lastPage = result.data.last_page;
					catalog.opinions.isFirst = result.data.prev_page_url == null;
					catalog.opinions.isLast = result.data.next_page_url == null;
					catalog.opinions.from = result.data.from;
					catalog.opinions.to = result.data.to;
					catalog.opinions.total = result.data.total;
				},
				function (error) {
					console.log(error);
				}
				);
		}

	}

	catalog.opinions.goToNext = function () {
		catalog.opinions.goToPage(catalog.opinions.currentPage + 1);
	}

	catalog.opinions.goToPrev = function () {
		catalog.opinions.goToPage(catalog.opinions.currentPage - 1);
	}

	catalog.opinions.goToFirst = function () {
		catalog.opinions.goToPage(1);
	}

	catalog.opinions.goToLast = function () {
		catalog.opinions.goToPage(catalog.opinions.lastPage);
	}

	catalog.opinions.getData = function () {
		return catalog.opinions.data;
	}

	catalog.opinions.getCurrentPage = function () {
		return catalog.opinions.currentPage;
	}

	catalog.opinions.getLastPage = function () {
		return catalog.opinions.lastPage;
	}

	catalog.opinions.setOrderField = function (field) {
		catalog.opinions.orderField = field;
	}

	catalog.opinions.setOrderType = function (type) {
		catalog.opinions.orderType = type;
	}

	catalog.setSearchText = function (text) {
		catalog.searchText = text;
	}

	return catalog;

}]);