app.factory('TicketService', ['$http', 'API_URL', function($http, apiUrl) { 

	return {

		///////////
		//Ticket//
		/////////
		
		index: function() {

			return $http.get(apiUrl + '/ticket');

		},

        indexSelf: function() {

			return $http.get(apiUrl + '/ticket-self');

		},

		show: function(id) {

			return $http.get(apiUrl + '/ticket/' + id);

		},

		showSelf: function(id) {

			return $http.get(apiUrl + '/ticket-self/' + id);

		},

		store: function(ticket) {

			return $http.post(apiUrl + '/ticket', ticket);

		},

		update: function(ticket) {

			return $http.put(apiUrl + '/ticket/' + ticket.id, ticket);

		},

		updateSelf: function(ticket) {

			return $http.put(apiUrl + '/ticket-self/' + ticket.id, ticket);

		},

		destroy: function(id) {

			return $http.delete(apiUrl + '/ticket/' + id);

		},

		cancelSelf: function(id) {

			return $http.delete(apiUrl + '/ticket-self/' + id);

		},


        ///////////////////
        //ticketcategory//
        ///////////////////
        
        indexTicketCategory: function() {
        
        return $http.get(apiUrl + '/ticketcategory');
        
        },
        
        showTicketCategory: function(id) {
        
        return $http.get(apiUrl + '/ticketcategory/' + id);
        
        },
        
        storeTicketCategory: function(ticketcategory) {
        
        return $http.post(apiUrl + '/ticketcategory', ticketcategory);
        
        },
        
        updateTicketCategory: function(ticketcategory) {
        
        return $http.put(apiUrl + '/ticketcategory/' + ticketcategory.id, ticketcategory);
        
        },
        
        destroyTicketCategory: function(id) {
        
        return $http.delete(apiUrl + '/ticketcategory/' + id);
        
        },

        ///////////////////
        //ticketpriority//
        ///////////////////
        
        indexTicketPriority: function() {
        
        return $http.get(apiUrl + '/ticketpriority');
        
        },
        
        showTicketPriority: function(id) {
        
        return $http.get(apiUrl + '/ticketpriority/' + id);
        
        },
        
        storeTicketPriority: function(ticketpriority) {
        
        return $http.post(apiUrl + '/ticketpriority', ticketpriority);
        
        },
        
        updateTicketPriority: function(ticketpriority) {
        
        return $http.put(apiUrl + '/ticketpriority/' + ticketpriority.id, ticketpriority);
        
        },
        
        destroyTicketPriority: function(id) {
        
        return $http.delete(apiUrl + '/ticketpriority/' + id);
        
        },
    
        ///////////////////
        //ticketanswer//
        ///////////////////
        
        indexTicketAnswer: function(ticketId) {
        
        return $http.get(apiUrl + '/ticketanswer/' + ticketId);
        
        },
        
        showTicketAnswer: function(id) {
        
        return $http.get(apiUrl + '/ticketanswer/' + id);
        
        },
        
        storeTicketAnswer: function(ticketanswer) {
        
        return $http.post(apiUrl + '/ticketanswer', ticketanswer);
        
        },
        
        updateTicketAnswer: function(ticketanswer) {
        
        return $http.put(apiUrl + '/ticketanswer/' + ticketanswer.id, ticketanswer);
        
        },
        
        destroyTicketAnswer: function(id) {
        
        return $http.delete(apiUrl + '/ticketanswer/' + id);
        
        },

        

	}

}]);