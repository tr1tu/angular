app.factory('CartService', ['OrderService', '$cookies', function(OrderService, $cookies) {

	var cart = {
		products : [],
		priceTotal: null
	};

	var update = function(result) {
		cart.products = result.data.cart.products;
		cart.setCartId(result.data.cartId);
		cart.priceTotal = result.data.cart.price_total;
	}

	cart.getCartId = function() {
		return $cookies.get('cart_id');
	}

	cart.setCartId = function(id) {
		var expireDate = new Date();
		expireDate.setDate(expireDate.getDate() + 30);
		$cookies.put('cart_id', id, { 'expires' : expireDate });
	}

	cart.unset = function() {
		$cookies.remove('cart_id');
		cart.products = [];
		cart.priceTotal = null;
	}

	cart.addProduct = function(productId, ammount, add) {
		add = add != null ? add : false;
		if(add)
			angular.forEach(cart.products, function(p) {
				if(p.id == productId)
					ammount += p.pivot.ammount;
			});

			OrderService.addToCart(cart.getCartId(), productId, ammount)
		.then(
				function(result) {
					update(result);
				},
				function(error) {
					console.log('Error while adding product');
				}
			);
	}

	cart.deleteProduct = function(productId) {
		cart.addProduct(productId, 0);
	}

	cart.loadProducts = function() {
		OrderService.getCart(cart.getCartId())
		.then(
				function(result) {
					update(result);
				},
				function(error) {
					console.log('Error while loading products');
				}
			);
	}


	

	return cart;

}]);