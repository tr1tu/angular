app.factory('AuthService', ['$auth', '$state', '$http', 'API_URL', '$rootScope', '$q', 'CartService', 'OrderService',
	function ($auth, $state, $http, API_URL, $rootScope, $q, CartService, OrderService) {
		return {

			adminRoles: { 0 : 'Admin'},
			authenticated : function() {
				return this.getUser() != null;
			},

			login: function (email, password) {

				var credentials = {
					email: email,
					password: password
				}

				var deferred = $q.defer();

				$auth.login(credentials).then(function () {


					$http.get(API_URL + '/authenticate/user')
						.then(function (response) {

							// Stringify the returned data to prepare it
							// to go into local storage
							var user = JSON.stringify(response.data.user);

							// Set the stringified user data into local storage
							localStorage.setItem('user', user);

							// The user's authenticated state gets flipped to
							// true so we can now show parts of the UI that rely
							// on the user being logged in
							this.authenticated = true;

							//Copy guest cart to user cart and load.
							if (CartService.getCartId()) {
								OrderService.getCart(CartService.getCartId())
									.then(
									function (result) {
										return OrderService.copyCart(CartService.getCartId(), result.data.cartId);
									}
									)
									.then(
									function (result) {
										CartService.loadProducts();
									}
									);
							}
							else {
								CartService.loadProducts();
							}


							// Everything worked out so we can now redirect to
							// the users state to view the data
							$state.go('home');
							deferred.resolve('Success');

						});

					


					// Handle errors
				}, function (error) {
					//  $scope.loginError = true;
					// if(error.data != null) $scope.loginErrorText = error.data.error;

					deferred.reject(error);



				});
				return deferred.promise;

			},

			logout: function () {

				var deferred = $q.defer();

				$auth.logout().then(function () {

					// Remove the authenticated user from local storage
					localStorage.removeItem('user');

					// Flip authenticated to false so that we no longer
					// show UI elements dependant on the user being logged in
					//this.authenticated = false;

					//Unset cart
					CartService.unset();

					$state.go('home');
					deferred.resolve();
				});

				return deferred.promise;
			},


			getUser: function () {
				return JSON.parse(localStorage.getItem('user'));
			},

			setUser : function(user) {
				localStorage.setItem('user', user);
			},

			userIsAdmin: function () {
				var user = this.getUser();
				if (!user)
					return false;

				roles = user.roles;
				found = false;

				angular.forEach(roles, function (value, key) {
					angular.forEach(this.adminRoles, function (r, k) {
						if (r == value.name)
							found = true;
					}, this);
				}, this);
				return found;
			}

		}
	}]);