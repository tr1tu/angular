app.factory('CustomModalService', ['$uibModal', function($uibModal) {

    return {

        uploadImage: function() {
            return $uibModal.open({
				templateUrl: 'app/views/templates/uploadmodal.html',
				size : 'lg',
				controller : ['$scope', 'FileService', 'MessagesService', function($scope, FileService, MS) {
					
					$scope.uploadImages = function (files) {
						if (files && files.length) {
							FileService.upload(files, 'public')
								.then(
								function (result) {
									$scope.uploadedImageUrl = result.data[0];
								},
								function (error) {
									MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
								}
								);
						}
					}



				}]
            });
        },

        imagePreview : function(field) {

            return $uibModal.open({
				templateUrl: 'app/views/templates/previewmodal.html',
				windowClass : 'modal-gray',
				size : 'lg',
				controller: ['$scope', 'field', function ($scope, field) {
					$scope.field = field;
				}],
				resolve: {
					field: function () {
						return field;
					}
				}

            });

        }

    };

}]);