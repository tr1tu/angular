app.factory('FileService', ['$http', 'API_URL', 'Upload', function($http, apiUrl, Upload) {

	return {

		upload : function(files, disk) {
			disk = disk || 'local';
			return Upload.upload({ url: apiUrl + '/upload-file', data: {files: files, disk : disk}});
		},

		download: function(filename) {
			return $http.get(apiUrl + '/download-file/' + filename);
		},

		downloadPublic: function(filename) {
			return $http.get(apiUrl + '/download-filep/' + filename);
		},

		getUrl : function(filename) {
			if(filename)
				return apiUrl + '/download-file/' + filename;
			else
				return "";
		},

		getUrlPublic : function(filename) {
			if(filename)
				return apiUrl + '/download-filep/' + filename;
			else
				return "";
		},

		delete : function(filenames) {
			return $http.post(apiUrl + '/delete-file', { filenames : filenames });
		},

		getExcelFields : function(filename) {
			return $http.post(apiUrl + '/get-excel-fields', { filename : filename });
		},

		getXmlNodeKeys : function(filename, subnodes) {
			return $http.post(apiUrl + '/get-xml-node-keys', { filename : filename, subnodes : subnodes });
		},

		saveResizedImages : function(files, width, height) {
			width = width || 200;
			height = height || 200;
			return $http.post(apiUrl + '/save-resized-images', { files : files, width: width, height : height });
		}

	};

}]);