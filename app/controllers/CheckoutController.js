app.controller('CheckoutController', ['$scope', 'CartService', 'CheckoutService', 'UserService', 'MessagesService', 'OrderService','$translate', 'ConfigService',
	function($scope, CartService, CheckoutService, UserService, MS, OrderService, $translate, ConfigService) {
	$scope.checkout = CheckoutService;
	$scope.cart = CartService;

	$scope.translated = {};

	var requiredTranslations = [
		'EDIT', 'CANCEL', 'SAVE', 'NEW_BILLING_ADDRESS', 'NEW_SHIPPING_ADDRESS', 'NAME', 'LASTNAME', 'DNICIF', 
		'COUNTRY', 'PROVINCE', 'CITY', 'ZIP_CODE', 'ADDRESS', 'PHONE', 'MOB_PHONE', 'DESCRIPTION'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.edit = translations.EDIT;
				$scope.translated.cancel = translations.CANCEL;
				$scope.translated.save = translations.SAVE;
				$scope.translated.new_billing_address = translations.NEW_BILLING_ADDRESS;
				$scope.translated.new_shipping_address = translations.NEW_SHIPPING_ADDRESS;
				$scope.translated.name = translations.NAME;
				$scope.translated.lastname = translations.LASTNAME;
				$scope.translated.dnicif = translations.DNICIF;
				$scope.translated.country = translations.COUNTRY;
				$scope.translated.province = translations.PROVINCE;
				$scope.translated.city = translations.CITY;
				$scope.translated.zip_code = translations.ZIP_CODE;
				$scope.translated.address = translations.ADDRESS;
				$scope.translated.phone = translations.PHONE;
				$scope.translated.mob_phone = translations.MOB_PHONE;
				$scope.translated.description = translations.DESCRIPTION;

				$scope.baTranslations = angular.copy($scope.translated);
				$scope.baTranslations.add = $scope.translated.new_billing_address;
				$scope.saTranslations = angular.copy($scope.translated);
				$scope.saTranslations.add = $scope.translated.new_shipping_address;

			}
		);

	//Default values
	$scope.paymentMethods = {
		'1' : 0.01,
		'2' : 0,
		'3' : 0,
		'4' : 0.05
	};

	ConfigService.getPaymentFees()
	.then(
		function (result) {
			$scope.paymentMethods['1'] = result.data.paypal_handling_fee;
			$scope.paymentMethods['2'] = result.data.card_handling_fee;
			$scope.paymentMethods['3'] = result.data.transfer_handling_fee;
			$scope.paymentMethods['4'] = result.data.cod_handling_fee;
			
		},
		function (error) {
			MS.showStatus(MS.getErrorsString(error.data), 'error');
		}
	);

	OrderService.indexShipment()
	.then(
			function(result) {
				$scope.shipments = result.data;
			}
		);

	$scope.getPriceTotal = function() {
		total = 0;

		if($scope.cart) total += parseFloat($scope.cart.priceTotal);
		if($scope.shipment) total += parseFloat($scope.shipment.fee);
		if($scope.cart && $scope.payment)
		{
			total += $scope.getPaymentCost();
		}

		return total;
	}

	$scope.getPaymentCost = function() {
		return $scope.paymentMethods[$scope.payment] * parseFloat($scope.cart.priceTotal);
	}

	$scope.finish = function () {
		if(!$scope.bAddress || !$scope.sAddress || !$scope.shipment || !$scope.payment)
		{
			return MS.showStatus('FORM_INCOMPLETE', 'error', 0);
		}

		$scope.checkout.data = {
			'bAddressId' : $scope.bAddress.id,
			'sAddressId' : $scope.sAddress.id,
			'shipmentId' : $scope.shipment.id,
			'paymentId' : $scope.payment

		};

		$scope.checkout.finish();
		$scope.finishing = true;
	}

}]);