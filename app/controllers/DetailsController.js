app.controller('DetailsController', ['$scope', 'MessagesService', 'CatalogService', '$state', 'ProductService', '$stateParams', '$translate', 
	'$rootScope', '$anchorScroll', 'CartService',
			 function($scope, MS, CatalogService, $state, ProductService, $stateParams, $translate, $rootScope, $anchorScroll, CartService) {

	$scope.catalog = CatalogService;
	$scope.currentUser = $scope.auth.getUser();

	if($stateParams.id) $scope.catalog.currentProduct = { id : $stateParams.id };

	if(!CatalogService.currentProduct)
	{
		$state.go('p_productlist');
		return;
	}

	CatalogService.opinions.goToPage(1);

	$scope.pageOptions = {
		currentPage : $scope.catalog.opinions.getCurrentPage,
		totalPages : $scope.catalog.opinions.getLastPage,
		nextPageFn : $scope.catalog.opinions.goToNext,
		prevPageFn : $scope.catalog.opinions.goToPrev,
		goPageFn : $scope.catalog.opinions.goToPage,
		goFirstFn : $scope.catalog.opinions.goToFirst,
		goLastFn : $scope.catalog.opinions.goToLast
	}


	$scope.changeOrder = function(field, type) {
		$scope.catalog.opinions.setOrderField(field);
		$scope.catalog.opinions.setOrderType(type);
		$scope.catalog.opinions.goToFirst();
	}

	$scope.selectOrderOptions = {

		orders : [],
		changeOrderFn : $scope.changeOrder,
	};

	$scope.translated = {};

	var requiredTranslations = [
		'SAVING', 'SAVED', 'DATE', 'SCORE', 'SELECT_ORDER', 'OUTSTANDING'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.saving = translations.SAVING;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.date = translations.DATE;
				$scope.translated.score = translations.SCORE;
				$scope.translated.outstanding = translations.OUTSTANDING;

				$scope.selectOrderOptions.orders = [	
					{ name : $scope.translated.date, type : 'asc', field : 'created_at' },
					{ name : $scope.translated.date, type : 'desc', field : 'created_at' },
					{ name : $scope.translated.score, type : 'asc', field : 'score' },
					{ name : $scope.translated.score, type : 'desc', field : 'score' },
					/*{ name : $scope.translated.outstanding, type : 'desc', field : 'upvotes' }*/ //No funciona bien

				];
				$scope.selectOrderOptions.placeholder = $scope.translated.select_order;
			}
		);

	

	ProductService.show(CatalogService.currentProduct.id)
		.then(
				function(result) {
					CatalogService.currentProduct = result.data;

					if(result.data)
						return ProductService.getRelated(CatalogService.currentProduct.id, 3);
				}
			)
		.then(
				function(result) {
					$scope.related = result.data;
				},
				function(error) {
					//MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
					$state.go('p_productlist');
					return;
				}
			);

	$scope.activeTab = 0;

	$scope.setActiveTab = function(tab) {
		$scope.activeTab = tab;
	}

	$scope.opine = function() {
		$scope.newOpinion = {};
	}

	$scope.saveOpinion = function(opinion) {

		MS.showStatus($scope.translated.saving, 'info', -1);

		if(!opinion.id)
		{
			opinion.user_id = $scope.auth.getUser().id;
			opinion.product_id = $scope.catalog.currentProduct.id;

			ProductService.storeOpinionSelf(opinion)
			.then(
					function(result) {
						MS.showStatus($scope.translated.saved);
						$scope.catalog.opinions.goToPage($scope.catalog.opinions.currentPage);
						$scope.newOpinion = null;
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
					}
				);
		}
		else
		{
			ProductService.updateOpinionSelf(opinion)
			.then(
					function(result) {
						MS.showStatus($scope.translated.saved);
						$scope.newOpinion = null;
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
					}
				);
		}
		
	}

	$scope.cancelOpinion = function() {
		$scope.newOpinion = null;
	}

	$scope.editOpinion = function(opinion) {
		$scope.newOpinion = opinion;
		$anchorScroll.yOffset = 300;
		$anchorScroll('opinionEdit');
	}

	$scope.setRecommended = function(value) {
		$scope.newOpinion.recommended = value;
	}

	$scope.upvoteOpinion = function(opinion) {

		ProductService.upvoteOpinion(opinion.id, $scope.auth.getUser().id)
		.then(
				function(result) {
					opinion.voters = result.data.voters;
					opinion.upvotes = result.data.upvotes;
					opinion.downvotes = result.data.downvotes;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);
	}

	$scope.downvoteOpinion = function(opinion) {

		ProductService.downvoteOpinion(opinion.id, $scope.auth.getUser().id)
		.then(
				function(result) {
					opinion.voters = result.data.voters;
					opinion.upvotes = result.data.upvotes;
					opinion.downvotes = result.data.downvotes;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);
	}

	$scope.addToCart = function() {
		CartService.addProduct($scope.catalog.currentProduct.id, 1);
	}

	$scope.buy = function() {
		$scope.addToCart();
		$state.go('cart');
	}



}]);