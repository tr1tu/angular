app.controller('pTicketController', ['$scope', '$stateParams', 'TicketService', 'MessagesService', '$state', '$translate',
    function ($scope, $stateParams, TicketService, MS, $state, $translate) {

        var ticketId = $stateParams.id;

        $scope.translated = {};

         var requiredTranslations = [
            'TICKET_CANCELLED', 'CONFIRM_CANCEL_TICKET'
        ];

        $translate(requiredTranslations)
            .then(
            function (translations) {
                $scope.translated.ticket_cancelled = translations.TICKET_CANCELLED;
                $scope.translated.confirm_cancel_ticket = translations.CONFIRM_CANCEL_TICKET;
            }
            );

        var load = function () {
            TicketService.showSelf(ticketId)
                .then(
                function (result) {
                    $scope.ticket = result.data;
                },
                function (error) {
                    $state.go('p_ticketlist');
                }
                );
        }

        load();

        $scope.openReply = function (opened) {
            opened = opened != null ? opened : true;
            $scope.replyOpened = opened;
        }

        $scope.sendReply = function (message) {
            if (message) {
                var ticketAnswer = {
                    message: message,
                    ticket_id: ticketId
                };

                TicketService.storeTicketAnswer(ticketAnswer)
                    .then(
                    function (result) {
                        $scope.message = "";
                        $scope.openReply(false);
                        load();
                    }
                    );
            }
        }

        $scope.cancelTicket = function(ticket) {

            if(!confirm($scope.translated.confirm_cancel_ticket))
                return;

            TicketService.cancelSelf(ticket.id)
            .then(
                function(result) {
                    MS.showStatus($scope.translated.ticket_cancelled, 'info', -1);
                    load();
                },
                function(error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
                }
            );
        }

    }]);