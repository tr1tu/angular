app.controller('pProductController', ['$scope', 'MessagesService', 'CatalogService', 'ProductService', '$translate', '$stateParams', 'CartService',
	function ($scope, MS, CatalogService, ProductService, $translate, $stateParams, CartService) {

		$scope.catalog = CatalogService;

		//Top anchor to scroll when paginate
		$scope.catalog.topAnchor = 'top';

		$scope.pageOptions = {
			currentPage: $scope.catalog.getCurrentPage,
			totalPages: $scope.catalog.getLastPage,
			nextPageFn: $scope.catalog.goToNext,
			prevPageFn: $scope.catalog.goToPrev,
			goPageFn: $scope.catalog.goToPage,
			goFirstFn: $scope.catalog.goToFirst,
			goLastFn: $scope.catalog.goToLast

		};



		$scope.setCategory = function (category) {

			if (category == null) {
				$scope.catalog.setTags([]);
				$scope.catalog.setCategory(category);
				$scope.catalog.goToFirst();

			}
			else {
				if (category.id != $scope.catalog.selectedCategoryId) {
					$scope.catalog.setTags([]);
					$scope.catalog.setCategory(category.id);
					$scope.catalog.goToFirst();

				}
			}


		}

		$scope.categoryOptions = {
			setCategory: $scope.setCategory,
			categories: null,
			my_tree: {}
		}

		/*$scope.search = function(keywords) {
			$scope.catalog.setCategory(null);
			$scope.catalog.setTags(keywords);
			$scope.catalog.goToFirst();
			$scope.categoryOptions.my_tree.select_branch(null);
		}*/

		$scope.search = function (searchText) {
			$scope.selectedOrder = { name: $scope.translated.relevance, type: 'desc', field: 'relevance' };
			$scope.catalog.setSearchText(searchText);
			$scope.catalog.goToFirst();
		}

		$scope.searchKeywordOptions = {
			searchFn: $scope.search
		}

		$scope.changeOrder = function (field, type) {
			//Avoid firing event when page loads
			if (field != $scope.catalog.getOrderField() || type != $scope.catalog.getOrderType()) {
				$scope.catalog.setOrderField(field);
				$scope.catalog.setOrderType(type);
				$scope.catalog.goToFirst();
			}
		}

		$scope.selectOrderOptions = {

			orders: [],
			changeOrderFn: $scope.changeOrder,
		};


		$scope.translated = {};

		var requiredTranslations = [
			'ALL_PRODUCTS', 'SEARCH_PRODUCT', 'NAME', 'PRICE', 'SELECT_ORDER', 'RELEVANCE'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.all_products = translations.ALL_PRODUCTS;
				$scope.translated.search_product = translations.SEARCH_PRODUCT;
				$scope.translated.name = translations.NAME;
				$scope.translated.price = translations.PRICE;
				$scope.translated.select_order = translations.SELECT_ORDER;
				$scope.translated.relevance = translations.RELEVANCE;

				$scope.categoryOptions.allProductsText = $scope.translated.all_products;
				$scope.searchKeywordOptions.placeholderText = $scope.translated.search_product;


				$scope.selectOrderOptions.orders = [
					{ name: $scope.translated.relevance, type: 'desc', field: 'relevance' },
					{ name: $scope.translated.name, type: 'asc', field: 'name' },
					{ name: $scope.translated.name, type: 'desc', field: 'name' },
					{ name: $scope.translated.price, type: 'asc', field: 'price' },
					{ name: $scope.translated.price, type: 'desc', field: 'price' }
				];
				$scope.selectOrderOptions.placeholder = $scope.translated.select_order;

				//Default order by relevance
				$scope.selectedOrder = { name: $scope.translated.relevance, type: 'desc', field: 'relevance' };

			}
			);

		$scope.categories = [];

		ProductService.indexCategoryNoChildren()
			.then(
			function (result) {
				$scope.categories = result.data;
				$scope.categoryOptions.categories = $scope.categories;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);

		$scope.selectedTags = [];

		$scope.$watch('selectedTags', function (newValue, oldValue) {
			if (newValue != oldValue) {
				$scope.catalog.setTags($scope.selectedTags);
				$scope.catalog.goToFirst();
			}
		}, true);

		$scope.addToCart = function (id) {
			CartService.addProduct(id, 1, true);
		}


	}]);