app.controller('ChangePasswordController', ['$scope', 'UserService', 'AuthService', 'MessagesService', '$state',
    function ($scope, UserService, AuthService, MS, $state) {
        $scope.user = {};

        $scope.changePassword = function () {
            $scope.user.id = AuthService.getUser().id;
            $scope.user.email = AuthService.getUser().email;
            UserService.updateSelf($scope.user)
                .then(
                function (result) {
                    var user = AuthService.getUser();
                    user.expired_password = 0;
                    AuthService.setUser(JSON.stringify(user));
                    $state.go('home');
                },
                function (error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error');
                });
        }
    }]);