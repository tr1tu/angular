app.controller('pTicketListController', ['$scope', 'TicketService', 'MessagesService', '$translate', '$state',
    function ($scope, TicketService, MS, $translate, $state) {

        $scope.translated = {};

        var requiredTranslations = [
            'TICKET_SENT'
        ];

        $translate(requiredTranslations)
            .then(
            function (translations) {
                $scope.translated.ticket_sent = translations.TICKET_SENT;
            }
            );

        TicketService.indexSelf()
            .then(
            function (result) {
                $scope.tickets = result.data.reverse();
            }
            )

        TicketService.indexTicketCategory()
            .then(
            function (result) {
                $scope.categories = result.data;
            }
            );

        TicketService.indexTicketPriority()
            .then(
            function (result) {
                $scope.priorities = result.data;
            }
            );

        $scope.openTicket = function (opened) {
            opened = opened || true;
            if(opened)
                $scope.ticket = {};
            else
                $scope.ticket = null;
        }

        $scope.sendTicket = function (ticket) {
            TicketService.store(ticket)
                .then(
                function (result) {
                    $state.go('p_ticketdetails', { id : result.data.id });
                    MS.showStatus($scope.translated.ticket_sent, 'info', -1);
                },
                function (error) {
                    MS.showStatus(MS.getErrorsString(error.data, 'error', -1));
                }
                );
        }

    }]);