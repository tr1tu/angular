app.controller('AuthController', ['$scope', 'AuthService', 'MessagesService', 'UserService', 'vcRecaptchaService',
			function ($scope, AuthService, MS, UserService, vcRecaptchaService) {

				
	/******* LOGIN ********/

	$scope.login = function(email, password) {

		AuthService.login(email, password).then(
			function(result) {
				
			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
			});


	}



	/******* REGISTER ********/

	$scope.newUser = {};

	$scope.register = function(user) {
		user['g-recaptcha-response'] = $scope.recaptchaResponse;
		UserService.register(user)
		.then(
			function(result) {
				$scope.login(user.email, user.password);
			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
				vcRecaptchaService.reload($scope.widgetId);
			}
		);
	}

	$scope.setCaptchaWidgetId = function(id) {
		$scope.widgetId = id;
	}


}]);