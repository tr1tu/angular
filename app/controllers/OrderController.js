app.controller('pOrderController', ['$scope', '$stateParams', '$state', 'OrderService', 'ConfigService', '$window', 'MessagesService',
	function($scope, $stateParams, $state, OrderService, ConfigService, $window, MS) {

	var orderId = $stateParams.id;

	if(!orderId)
		$state.go('orderlist');

	$scope.createDate = new Date();
	$scope.updateDate = new Date();

	OrderService.showSelfOrder(orderId)
	.then(
			function(result) {
				$scope.order = result.data;
				if(!$scope.order) $state.go('orderlist');

				//Get dates
				$scope.createDate = new Date($scope.order.created_at);
				$scope.updateDate = new Date($scope.order.updated_at);

				$scope.pages = [];
				for(var i = 0; i < $scope.order.items.length; i++)
				{
					var index = Math.floor(i / 8);
					if(!$scope.pages[index]) $scope.pages[index] = [];
					$scope.pages[index].push($scope.order.items[i]);
					$scope.pages[index].id = index;
				}

				//COD
				if($scope.order.p_id == 4)
					return ConfigService.getOrderStatusCOD();
				else
					return ConfigService.getOrderStatus();

			}
		)
		.then(
			function (result) {
				$scope.statusList = result.data;
			}
		)
		.catch(
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
		);

	ConfigService.getCompanyData()
	.then(
			function(result) {
				$scope.company = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
		);

	ConfigService.getPaymentFinishedSteps()
	.then(
		function (result) {
			$scope.paymentFinishedSteps = result.data;
		},
		function (error) {
			MS.showStatus(MS.getErrorsString(error.data), 'error');
		}
	);

	$scope.finishPayment = function() {
		OrderService.finishPaymentOrder($scope.order.id)
		.then(
				function(result) {
					$window.location.href = decodeURIComponent(result.data);
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
			);
	}

	$scope.isInvoiceCreated = function() {
		if(!$scope.order)
			return false;

		return $scope.order.invoice != null;
	}

}]);