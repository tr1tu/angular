app.controller('CartController', ['$scope', 'CatalogService', function($scope, CatalogService) {

	//We don't need this because cart is defined in CommonController.
	//$scope.cart = CartService;


	$scope.cartDetails = function(product) {
		CatalogService.showProduct(product);
	}



}]);