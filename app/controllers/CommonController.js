app.controller('CommonController', ['$scope', 'CartService', 'AuthService', 'MessagesService', 'ConfigService',
	function ($scope, CartService, AuthService, MS, ConfigService) {
		$scope.cart = CartService;
		$scope.auth = AuthService;
		$scope.ms = MS;

		//////////
		//SLIDE//
		////////

		$scope.myInterval = 5000;
		$scope.noWrapSlides = false;
		$scope.active = 0;
		$scope.slides = [];

		ConfigService.indexActiveSlide()
		.then(
			function(result) {
				$scope.slides = result.data;
			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5);
			}
		);


		/////////////////////
		////CONFIG DATA/////
		///////////////////

		ConfigService.getCompanyData()
		.then(
			function (result) {
				$scope.companyData = result.data;
			}
		);


		ConfigService.getSocialMedia()
		.then(
			function (result) {
				$scope.socialMedia = result.data;
			}
		);

	}]);