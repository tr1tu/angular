app.controller('pOrderListController', ['$scope', 'OrderService', function($scope, OrderService) {
	OrderService.indexSelfOrder()
	.then(
			function(result) {
				$scope.orders = result.data;
			}
		);
}]);