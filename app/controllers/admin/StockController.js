app.controller('StockController', ['$scope', 'ProductService', 'MessagesService', '$translate', 
	function($scope, ProductService, MS, $translate) {

	$scope.productGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {

			$scope.gridApiProduct = gridApi;
		},

	};

	$scope.sizingGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {

			$scope.gridApiSizing = gridApi;
		}

	};

	$scope.sizeGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,
		enableCellEditOnFocus : true,


		onRegisterApi : function(gridApi) {
			$scope.gridApiSize = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
       			{
	       				$scope.updateStock(rowEntity);
       			}
       		});
		}

	};

	$scope.translated = {};

	var requiredTranslations = [
		'NAME', 'PRICE', 'DESCRIPTION', 'REFERENCE', 'STOCK', 'SAVED', 'SAVING', 'EDIT', 'SELECT', 'SIZING_NUMBER', 'SIZE'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.price = translations.PRICE;
				$scope.translated.description = translations.DESCRIPTION;
				$scope.translated.reference = translations.REFERENCE;
				$scope.translated.stock = translations.STOCK;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.edit = translations.EDIT;
				$scope.translated.select = translations.SELECT;
				$scope.translated.sizing_number = translations.SIZING_NUMBER;
				$scope.translated.size = translations.SIZE;

				$scope.productGridOptions.columnDefs = [
					{ field : 'name', name : $scope.translated.name },
					{ field : 'price', name : $scope.translated.price },
					{ field : 'description', name : $scope.translated.description },
					{ field : 'reference', name : $scope.translated.reference },
					{ name : $scope.translated.select,  width: 90, enableFiltering: false,
			cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.selectProduct(row.entity)" translate>SELECT</button>' }

				];

				$scope.sizingGridOptions.columnDefs = [
					{ field : 'name', name : $scope.translated.name },
					{ field : 'sizing_number', name : $scope.translated.sizing_number },
					{ name : $scope.translated.select,  width: 120, enableFiltering: false, enableCellEdit : false,
			cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.selectSizing(row.entity)" translate>SELECT</button>' }

				];

				$scope.sizeGridOptions.columnDefs = [
					{ field : 'size', name : $scope.translated.size, enableCellEdit: false },
					{ field : 'stock', name : $scope.translated.stock }

				];

			}
		);


	ProductService.index()
    	.then(function(result) {

    		$scope.products = result.data;
    		$scope.productGridOptions.data = $scope.products;

	});


    $scope.selectProduct = function(product) {
    	ProductService.show(product.id)
    	.then(
    			function(result) {
    				$scope.currentProduct = result.data;
    				if($scope.currentProduct.sizings.length == 1)
    					$scope.selectSizing($scope.currentProduct.sizings[0]);
    				else
    					$scope.currentSizing = {};
    				$scope.sizingGridOptions.data = $scope.currentProduct.sizings;
    			},
    			function(error) {
    				MS.showStatus(MS.getErrorsString(error.data), 'error');
    			}
    		);
    	
    }

    $scope.selectSizing = function(sizing) {
    	ProductService.showSizing(sizing.id)
    	.then(
    			function(result) {
    				$scope.currentSizing = result.data;
    				angular.forEach($scope.currentSizing.sizes, function(v,k) {
    					ProductService.showStock($scope.currentProduct.id, v.id)
    					.then(
    							function(result) {
				    				v.stock = result.data.stock;
				    			},
				    			function(error) {
				    				MS.showStatus(MS.getErrorsString(error.data), 'error');
				    			}	
    						);
    				});
    				$scope.sizeGridOptions.data = $scope.currentSizing.sizes;
    			},
    			function(error) {
    				MS.showStatus(MS.getErrorsString(error.data), 'error');
    			}
    		);
    }

    $scope.updateStock = function(row) {

    	//The row received is a Size Row with stock field added. 
    	//We want to save a Stock so we have to create the stock object from this row.

    	MS.showStatus($scope.translated.saving, 'info', -1);
    	ProductService.showStock($scope.currentProduct.id, row.id)
    	.then(
    			function(result) {

    				if(result.data)
    				{
    					//Update
						stock = { 
							id : result.data.id,
							product_id : result.data.product_id,
							size_id : result.data.size_id,
							stock : row.stock
						};
    					ProductService.updateStock(stock)
    					.then(
		    					function(result) {
		    						MS.showStatus($scope.translated.saved);
		    					},
		    					function(error) {
		    						MS.showStatus(MS.getErrorsString(error.data), 'error');
		    					}
    						);
    				}
    				else
    				{
    					//Store

						stock = {
							product_id : $scope.currentProduct.id,
							size_id : row.id,
							stock : row.stock
						};

    					ProductService.storeStock(stock)
    					.then(
		    					function(result) {
		    						MS.showStatus($scope.translated.saved);

		    					},
		    					function(error) {
		    						MS.showStatus(MS.getErrorsString(error.data), 'error');
		    					}
    						);
    				}

    			},
    			function(error) {
    				MS.showStatus(MS.getErrorsString(error.data), 'error');
    			}
    		);

    }

    $scope.updateProduct = function(product) {

		MS.showStatus($scope.translated.saving, 'info');
	
		ProductService.update(product)
		.then(
				function(result) {
					MS.showStatus($scope.translated.saved);
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
			);
		
	

	};

}]);