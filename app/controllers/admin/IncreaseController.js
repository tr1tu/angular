app.controller('IncreaseController', ['$scope', 'ProductService', 'MessagesService', '$translate',
	function ($scope, ProductService, MS, $translate) {


		$scope.productGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {

				$scope.gridApiProduct = gridApi;
			},

		};

		$scope.sizingGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {

				$scope.gridApiSizing = gridApi;
			}

		};

		$scope.sizeGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,
			enableCellEditOnFocus: true,


			onRegisterApi: function (gridApi) {
				$scope.gridApiSize = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
					if (newValue != oldValue) {
						$scope.updateIncrease(rowEntity);
					}
				});
			}

		};

		$scope.translated = {};

		var requiredTranslations = [
			'NAME', 'PRICE', 'DESCRIPTION', 'REFERENCE', 'SAVED', 'SAVING', 'EDIT', 'SELECT', 'SIZING_NUMBER', 'SIZE', 'INCREASE', 'TYPE',
			'PLAIN', 'PERCENTAGE', 'TOTAL'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.price = translations.PRICE;
				$scope.translated.description = translations.DESCRIPTION;
				$scope.translated.reference = translations.REFERENCE;
				$scope.translated.stock = translations.STOCK;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.edit = translations.EDIT;
				$scope.translated.select = translations.SELECT;
				$scope.translated.sizing_number = translations.SIZING_NUMBER;
				$scope.translated.size = translations.SIZE;
				$scope.translated.increase = translations.INCREASE;
				$scope.translated.type = translations.TYPE;
				$scope.translated.plain = translations.PLAIN;
				$scope.translated.percentage = translations.PERCENTAGE;
				$scope.translated.total = translations.TOTAL;

				$scope.productGridOptions.columnDefs = [
					{ field: 'name', name: $scope.translated.name },
					{ field: 'price', name: $scope.translated.price },
					{ field: 'description', name: $scope.translated.description },
					{ field: 'reference', name: $scope.translated.reference },
					{
						name: $scope.translated.select, width: 90, enableFiltering: false,
						cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.selectProduct(row.entity)" value="' + $scope.translated.select + '" />'
					}

				];

				$scope.sizingGridOptions.columnDefs = [
					{ field: 'name', name: $scope.translated.name },
					{ field: 'sizing_number', name: $scope.translated.sizing_number },
					{
						name: $scope.translated.select, width: 120, enableFiltering: false, enableCellEdit: false,
						cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.selectSizing(row.entity)" value="' + $scope.translated.select + '" />'
					}

				];

				$scope.sizeGridOptions.columnDefs = [
					{ field: 'size', name: $scope.translated.size, enableCellEdit: false },
					{ field: 'increase', name: $scope.translated.increase },
					{
						field: 'type',
						name: $scope.translated.type,
						editType: 'dropdown',
						enableCellEdit: true,
						editableCellTemplate: 'ui-grid/dropdownEditor',
						editDropdownOptionsArray: [{ ID: 1, label: $scope.translated.plain }, { ID: 2, label: $scope.translated.percentage }],
						editDropdownIdLabel: 'ID',
						editDropdownValueLabel: 'label',
						cellFilter: "griddropdown:editDropdownOptionsArray:editDropdownIdLabel:editDropdownValueLabel:row.entity.relatedobject.name"
					},
					{ field: 'total', name: $scope.translated.total, enableCellEdit: false }

				];

			}
			);


		ProductService.index()
			.then(
			function (result) {

				$scope.products = result.data;
				$scope.productGridOptions.data = $scope.products;

			});


		$scope.selectProduct = function (product) {
			ProductService.show(product.id)
				.then(
				function (result) {
					$scope.currentProduct = result.data;
					if ($scope.currentProduct.sizings.length == 1)
						$scope.selectSizing($scope.currentProduct.sizings[0]);
					else
						$scope.currentSizing = {};
					$scope.sizingGridOptions.data = $scope.currentProduct.sizings;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
				}
				);

		}

		$scope.selectSizing = function (sizing) {
			ProductService.showSizing(sizing.id)
				.then(
				function (result) {
					$scope.currentSizing = result.data;
					angular.forEach($scope.currentSizing.sizes, function (v, k) {
						ProductService.showIncrease($scope.currentProduct.id, v.id)
							.then(
							function (result) {
								v.increase = result.data.increase;
								v.type = result.data.type;
								v.total = getTotalPrice(v.increase, v.type, $scope.currentProduct.price);
							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
							}
							);
					});
					$scope.sizeGridOptions.data = $scope.currentSizing.sizes;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
				}
				);
		}

		$scope.updateIncrease = function (row) {

			//The row received is a Size Row with type and increase fields added. 
			//We want to save a Increase so we have to create the increase object from this row.

			row.total = getTotalPrice(row.increase, row.type, $scope.currentProduct.price);

			MS.showStatus($scope.translated.saving, 'info', -1);
			ProductService.showIncrease($scope.currentProduct.id, row.id)
				.then(
				function (result) {

					if (result.data) {
						//Update
						increase = {
							id: result.data.id, product_id: result.data.product_id, size_id: result.data.size_id,
							increase: row.increase, type: row.type
						};
						ProductService.updateIncrease(increase)
							.then(
							function (result) {
								MS.showStatus($scope.translated.saved);
							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
							}
							);
					}
					else {
						//Store

						increase = {
							product_id: $scope.currentProduct.id, size_id: row.id, increase: row.increase,
							type: row.type
						};

						ProductService.storeIncrease(increase)
							.then(
							function (result) {
								MS.showStatus($scope.translated.saved);

							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
							}
							);
					}

				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
				}
				);

		}


		var getTotalPrice = function (increase, type, price) {

			if (!increase || !type || !price)
				return "";

			if (type == 1) {
				return parseFloat(price) + parseFloat(increase);
			}
			else {
				return parseFloat(price * (1 + increase / 100)).toFixed(2);
			}

		}

	}]);