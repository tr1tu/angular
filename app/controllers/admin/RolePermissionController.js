app.controller('RolePermissionController', ['$scope', 'UserService', '$q', '$filter', 'MessagesService', '$translate',
	function ($scope, UserService, $q, $filter, MS, $translate) {


		$scope.roleGridOptions = {};

		$scope.translated = {};
		
		var requiredTranslations = [
			'NAME', 'DISPLAY_NAME', 'DESCRIPTION', 'EDIT'
		];
		
		$translate(requiredTranslations)
		.then(
		function (translations) {
			$scope.translated.name = translations.NAME;
			$scope.translated.display_name = translations.DISPLAY_NAME;
			$scope.translated.description = translations.DESCRIPTION;
			$scope.translated.edit = translations.EDIT;

			$scope.roleGridOptions.columnDefs = [
				{ field: 'name', displayName: $scope.translated.name },
				{ field: 'display_name', displayName: $scope.translated.display_name },
				{ field: 'description', displayName: $scope.translated.description },
				{ name: 'Edit', displayName: $scope.translated.edit, width: 90, enableCellEdit: false, cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.editRole(row.entity)" translate>EDIT</button>' }
			];
		}
		);

		
		

		//$scope.roleGridOptions.enableFullRowSelection = true;
		//$scope.roleGridOptions.modifierKeysToMultiSelect = true;

		$scope.roleGridOptions.enableFiltering = true;
		$scope.roleGridOptions.enableGridMenu = true;

		$scope.roleGridOptions.onRegisterApi = function (gridApi) {
			$scope.gridApi = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
				if (newValue != oldValue)
					$scope.updateRole(rowEntity);
			});
		};

		UserService.indexRole()
			.then(function (result) {

				$scope.roles = result.data;
				$scope.roleGridOptions.data = $scope.roles;

			});

		$scope.permList = [];
		$scope.permListSelected = [];


		$scope.updateRole = function (role) {

			MS.showStatus('SAVING', 'info');

			UserService.showRole(role.id)
				.then(function (result) {
					//Estamos actualizando un rol ya existente.
					if (result.data) {
						UserService.updateRole(role)
							.then(
							function (result) {
								MS.showStatus('SAVED', 'info');
							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error');
							});
					}
					else	//Estamos añadiendo un nuevo rol
					{
						UserService.storeRole(role)
							.then(
							function (result) {
								role.id = result.data.id;

								MS.showStatus('SAVED', 'info');

							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error');
							});

					}
				});



		};

		$scope.roleCreate = function () {
			role = { name: 'New role' };
			$scope.roles.push(role);
		};

		$scope.deleteSelected = function () {

			if (!confirm($translate.instant('REMOVE_ROLE', { count : $scope.gridApi.selection.getSelectedCount()})))
				return;

			var selRows = $scope.gridApi.selection.getSelectedRows();

			angular.forEach(selRows, function (value, key) {
				index = $scope.roles.indexOf(value);
				$scope.roles.splice(index, 1);
				UserService.destroyRole(value.id);
				$scope.gridApi.selection.clearSelectedRows();
			});

		};


		var getOtherPerm = function (perms) {

			var otherPerm = [];

			var deferred = $q.defer();

			return UserService.indexPermission()
				.then(function (allPerm) {

					angular.forEach(allPerm.data, function (value, key) {
						var found = false;

						angular.forEach(perms, function (v, k) {
							if (v.name == value.name)
								found = true;
						});

						if (!found) {
							otherPerm.push(value);
						}
					});

					deferred.resolve(otherPerm);
					return deferred.promise;

				},
				function (error) {
					deferred.reject(error);
					return deferred.promise;


				});


		};

		$scope.editRole = function (row) {

			$scope.editingRole = row;
			$scope.permList = [];
			$scope.permListSelected = [];

			UserService.showRole(row.id)
				.then(function (result) {
					$scope.permListSelected = result.data.perms;

					getOtherPerm(result.data.perms).then(function (data) {
						if (data != null) $scope.permList = $scope.permList.concat(data);
						$scope.editing = true;
					});


				});

		};

		$scope.savePerm = function () {

			MS.showStatus('SAVING', 'info');

			UserService.clearPermissions($scope.editingRole.id)
				.then(function (result) {

					angular.forEach($scope.permListSelected, function (value, key) {

						UserService.attachPermission($scope.editingRole.id, value.id)
							.then(function () {

								MS.showStatus('SAVED', 'info');

							});

					});

					if ($scope.permListSelected.length == 0)
						MS.showStatus('SAVED', 'info');

				});



		};

	}]);