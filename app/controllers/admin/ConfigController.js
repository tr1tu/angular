app.controller('ConfigController', ['$scope', 'ConfigService', 'FileService', '$translate', 'MessagesService', '$uibModal', 'CustomModalService', 'OrderService',
	function ($scope, ConfigService, FileService, $translate, MS, $uibModal, CMService, OrderService) {


		$scope.temp = {};

		$scope.translated = {};

		var requiredTranslations = [
			'SAVED', 'NAME', 'FEE'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.saved = translations.SAVED;
				$scope.translated.name = translations.NAME;
				$scope.translated.fee = translations.FEE;


				$scope.shipmentGridOptions.columnDefs = [
					{ field: 'name', displayName: $scope.translated.name },
					{ field: 'fee', displayName: $scope.translated.fee },
					{
						name: 'edit', displayName: $scope.translated.edit, width: 60, enableFiltering: false, enableCellEdit: false,
						cellTemplate: '<btn class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.shipmentEdit(row.entity)" translate>EDIT</button>'
					}

				];


			}
			);


		$scope.config = {};

		ConfigService.index()
			.then(
			function (result) {
				$scope.config = result.data;
				loadOrderStatusList($scope.orderStatusList, result.data, 'order_status_text_', result.data.order_status_steps);
				loadOrderStatusList($scope.orderStatusCODList, result.data, 'order_status_cod_text_', result.data.order_status_cod_steps);
				$scope.temp.payment_finished_step_name = getPaymentStepName($scope.orderStatusList, $scope.config.payment_finished_step);
				$scope.temp.payment_finished_step_name_cod = getPaymentStepName($scope.orderStatusCODList, $scope.config.payment_finished_step_cod);
			}
			);

		$scope.saveConfig = function () {
			//Add order status to config object.
			addStatusKeysToObject($scope.orderStatusList, $scope.config, 'order_status_text_');
			$scope.config.order_status_steps = $scope.orderStatusList.length;
			addStatusKeysToObject($scope.orderStatusCODList, $scope.config, 'order_status_cod_text_');
			$scope.config.order_status_cod_steps = $scope.orderStatusCODList.length;
			$scope.config.payment_finished_step = getPaymentStepId($scope.orderStatusList, $scope.temp.payment_finished_step_name);
			$scope.config.payment_finished_step_cod = getPaymentStepId($scope.orderStatusCODList, $scope.temp.payment_finished_step_name_cod);

			ConfigService.setMulti({ config: $scope.config })
				.then(
				function (result) {

					//Save slides
					var data = { slides: $scope.slides.lists.current };
					return ConfigService.storeMultiSlide(data);
				}
				)
				.then(
				function (result) {

				}
				)
				.catch(function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
				})
				.finally(function () {
					MS.showStatus($scope.translated.saved, 'info', 5000);
				});

		}

		///////////
		//MODALS//
		/////////
		$scope.uploadModal = {};
		$scope.previewModal = {};
		$scope.editModal = {};

		$scope.openPreviewModal = function (field) {

			$scope.previewModal = CMService.imagePreview(field).result.then(function () { }, function (res) { });
		}

		$scope.openUploadModal = function (options, fieldname) {

			$scope.uploadModal = CMService.uploadImage().result.then(function (result) {
				options[fieldname] = result;
			}, function (result) { });

		}

		$scope.openEditModal = function (item) {

			$scope.editModal = $uibModal.open({
				templateUrl: 'editmodal.html',
				size: 'lg',
				backdrop: 'static',
				controller: ['$scope', 'text', function ($scope, text) {
					$scope.text = text;
				}],
				resolve: {
					text: function () {
						return item.text;
					}
				}
			}).result.then(function (result) {
				item.text = result;
			}, function (result) { });
		}

		//////////
		//SLIDE//
		////////

		$scope.myInterval = 5000;
		$scope.noWrapSlides = false;
		$scope.active = 0;
		$scope.lastOrder = -1;

		$scope.slides = {
			selected: null,
			lists: {
				"current": []
			},
			templates: []
		};

		ConfigService.indexSlide()
			.then(
			function (result) {
				$scope.slides.lists.current = result.data;
				if (result.data.length > 0)
					$scope.lastOrder = result.data[result.data.length - 1].order;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);



		$scope.saveSlides = function () {
			var data = { slides: $scope.slides.lists.current };
			ConfigService.storeMultiSlide(data)
				.then(
				function (result) {
					MS.showStatus($scope.translated.saved, 'info', 5000);
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
				}
				);
		}

		$scope.addSlide = function () {
			$scope.slides.lists.current.push({
				opened: true,
				shown: true,
				order: ++($scope.lastOrder),
				active: 1,
				start_date: new Date()
			});
		}

		$scope.reorderSlides = function () {
			var order = 0;
			angular.forEach($scope.slides.lists.current, function (slide) {
				slide.order = order++;
			});
			$scope.lastOrder = order - 1;
		}

		$scope.deleteSlide = function (slide) {
			$scope.slides.lists.current.splice(slide.order, 1);
			$scope.reorderSlides();
		}


		/////////////////////
		//SHIPMENT METHODS//
		///////////////////

		$scope.shipmentGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {

				$scope.gridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
					if (newValue != oldValue)
						$scope.updateShipment(rowEntity);
				});
			},

		};

		OrderService.indexShipment()
			.then(
			function (result) {
				$scope.shipments = result.data;
				$scope.shipmentGridOptions.data = $scope.shipments;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
			);

		$scope.updateShipment = function (shipment) {

			if (shipment.id) {
				//update
				OrderService.updateShipment(shipment)
					.then(
					function (result) {
						MS.showStatus('SAVED');
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
			else {
				//store
				OrderService.storeShipment(shipment)
					.then(
					function (result) {
						shipment.id = result.data.id;
						MS.showStatus('SAVED', 'info');
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}

		}

		$scope.shipmentEdit = function (shipment) {
			$scope.currentShipment = shipment;
		}

		$scope.shipmentCreate = function() {
			var s = {};
			$scope.shipments.push(s);
			$scope.currentShipment = s;
		}


		/////////////////
		//ORDER STATUS//
		///////////////
		$scope.orderStatusList = [];
		$scope.orderStatusCODList = [];

		function addStatusKeysToObject(arr, obj, prefix) {
			
			for(var i = 0; i < arr.length; i++) {
				obj[prefix + i] = arr[i];
			}

		}

		function loadOrderStatusList(list, config, prefix, steps) {
			
			steps = steps != null ? steps : 0;

			for(var i = 0; i < steps; i++) {
				list.push(config[prefix + i]);
			}
		}

		function getPaymentStepId(list, stepName)
		{
			if(list)
				for(var i = 0; i < list.length; i++) {
					if(list[i] == stepName)
						return i;
				}

			return -1;
		}

		function getPaymentStepName(list, stepId) {
			if(list && list.length > stepId)
				return list[stepId];

			return "";
		}

	}]);