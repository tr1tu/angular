app.controller('HomeController', ['$scope', 'AuthService', '$state', 'MessagesService',
	function($scope, AuthService, $state, MS) {

    $scope.logout = AuthService.logout;

    $scope.ms = MS;

}]);