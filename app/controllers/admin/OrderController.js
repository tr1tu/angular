app.controller('OrderController', ['$scope', 'OrderService', 'MessagesService', '$translate', 'ConfigService', '$anchorScroll', '$timeout',
	function ($scope, OrderService, MS, $translate, ConfigService, $anchorScroll, $timeout) {


		$scope.orderGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {

				$scope.gridApi = gridApi;

				/*gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
							if(newValue != oldValue)
									$scope.updateOrder(rowEntity);
						});*/
			},

		};


		$scope.translated = {};

		var requiredTranslations = [
			'LASTNAME', 'NAME', 'STATUS', 'TOTAL_PRICE', 'DATE', 'EDIT'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.lastname = translations.LASTNAME;
				$scope.translated.name = translations.NAME;
				$scope.translated.status = translations.STATUS;
				$scope.translated.total_price = translations.TOTAL_PRICE;
				$scope.translated.date = translations.DATE;
				$scope.translated.edit = translations.EDIT;


				$scope.orderGridOptions.columnDefs = [
					{ field: 'id', displayName: '#', maxWidth: 80 },
					{ field: 'ba_lastname', displayName: $scope.translated.lastname },
					{ field: 'ba_name', displayName: $scope.translated.name },
					{ field: 'status.name', displayName: $scope.translated.status, maxWidth: 150 },
					{ field: 'price_total_with_fees', displayName: $scope.translated.total_price, maxWidth: 80 },
					{ field: 'created_at', displayName: $scope.translated.date, maxWidth: 165 },
					{
						name: 'edit', displayName: $scope.translated.edit, width: 60, enableFiltering: false, enableCellEdit: false, enableSorting: false,
						cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.orderEdit(row)">' + $scope.translated.edit + '</button>'
					}

				];

			}
			);

		OrderService.indexOrder()
			.then(
			function (result) {
				$scope.orders = result.data;
				$scope.orderGridOptions.data = $scope.orders;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);

		ConfigService.getCompanyData()
			.then(
			function (result) {
				$scope.company = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);

		ConfigService.getOrderStatus()
			.then(
			function (result) {
				$scope.statusCommonList = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);


		ConfigService.getOrderStatusCOD()
			.then(
			function (result) {
				$scope.statusCODList = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
			}
			);


		$scope.updateOrder = function (order) {

			if (order.id) {
				//update
				OrderService.updateOrder(order)
					.then(
					function (result) {
						MS.showStatus('SAVED');
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
					}
					);
			}

		}


		$scope.orderEdit = function (order) {
			OrderService.showOrder(order.entity.id)
				.then(
				function (result) {
					order.entity = result.data;
					$scope.currentOrder = order.entity;

					//COD
					if($scope.currentOrder.p_id == 4)
						$scope.statusList = $scope.statusCODList;
					else
						$scope.statusList = $scope.statusCommonList;

					$timeout(function () {
						$anchorScroll.yOffset = 0;
						$anchorScroll('orderInfo');
					});
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
				);

		}


		$scope.paymentCreate = function () {
			var payment = {};
			$scope.payments.push(payment);
			$scope.currentPayment = payment;
		}

		$scope.getCreateDate = function () {
			if (!$scope.currentOrder)
				return null;
			var d = new Date($scope.currentOrder.created_at);
			return d;
		}

		$scope.getUpdateDate = function () {
			if (!$scope.currentOrder)
				return null;
			var d = new Date($scope.currentOrder.updated_at);
			return d;
		}


		$scope.changeStatus = function (step) {
			OrderService.updateStatusOrder($scope.currentOrder.id, step.id)
				.then(
				function (result) {
					$scope.currentOrder.status = result.data;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);
		}

		$scope.isInvoiceCreated = function () {
			if (!$scope.currentOrder)
				return false;

			return $scope.currentOrder.invoice != null;
		}

		$scope.openMessage = function (open) {
			open = open != null ? open : true;
			$scope.messageOpened = open;
		}

		$scope.sendMessage = function (message) {
			OrderService.sendCustomerEmailOrder(message, $scope.currentOrder.user.id)
				.then(
				function (result) {
					MS.showStatus('EMAIL_SENT');
					$scope.message = "";
					$scope.openMessage(false);
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
				);
		}

	}]);