app.controller('UserController', ['$scope', 'API_URL', 'UserService', 'MessagesService', '$q', '$filter', '$stateParams', '$translate',
	function ($scope, apiUrl, UserService, MS, $q, $filter, $stateParams, $translate) {

		if ($stateParams.id) {
			UserService.show($stateParams.id)
				.then(
				function (result) {
					$scope.currentUser = result.data;
				}
				);
		}

		//Cambia la clase del header al filtrar por una columna
		$scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
			if (col.filters[0].term) {
				return 'header-filtered';
			}
			else {
				return '';
			}
		};

		$scope.userGridOptions = {};

		$scope.translated = {};

		var requiredTranslations = [
			'ID', 'USERNAME', 'NAME', 'LASTNAME', 'DNICIF',
			'PHONE', 'MOBPHONE', 'FAX', 'COUNTRY', 'EMAIL',
			'EDIT'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.id = translations.ID;
				$scope.translated.username = translations.USERNAME;
				$scope.translated.name = translations.NAME;
				$scope.translated.lastname = translations.LASTNAME;
				$scope.translated.dnicif = translations.DNICIF;
				$scope.translated.phone = translations.PHONE;
				$scope.translated.mob_phone = translations.MOB_PHONE;
				$scope.translated.fax = translations.FAX;
				$scope.translated.country = translations.COUNTRY;
				$scope.translated.email = translations.EMAIL;
				$scope.translated.edit = translations.EDIT;

				$scope.userGridOptions.columnDefs = [
					{ field: 'id', displayName: $scope.translated.id, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'username', displayName: $scope.translated.username, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'name', displayName: $scope.translated.name, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'lastname', displayName: $scope.translated.lastname, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'dnicif', displayName: $scope.translated.dnicif, headerCellClass: $scope.highlightFilteredHeader, visible: false },
					{ field: 'phone', displayName: $scope.translated.phone, headerCellClass: $scope.highlightFilteredHeader, visible: false },
					{ field: 'mobphone', displayName: $scope.translated.mobphone, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'fax', displayName: $scope.translated.fax, headerCellClass: $scope.highlightFilteredHeader, visible: false },
					{ field: 'country', displayName: $scope.translated.country, headerCellClass: $scope.highlightFilteredHeader },
					{ field: 'email', displayName: $scope.translated.email, headerCellClass: $scope.highlightFilteredHeader, visible: false },
					{
						name: 'edit', displayName: $scope.translated.edit, enableFiltering: false, width: 120,
						cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.editUser(row.entity)" translate>EDIT</button>'
					}
		
				];
			}
			);

		
		

		$scope.userGridOptions.enableFiltering = true;
		$scope.userGridOptions.enableGridMenu = true;

		UserService.index()
			.then(function (result) {
				$scope.users = result.data;
				$scope.userGridOptions.data = $scope.users;
			});




		$scope.userGridOptions.onRegisterApi = function (gridApi) {
			$scope.gridApi = gridApi;
		};

		$scope.addressGridOptions = {
			columnDefs: [
				{ field: 'description', displayname: 'Description' },
				{ field: 'country', displayname: 'Country' },
				{ field: 'province', displayname: 'Province' },
				{ field: 'city', displayname: 'City' },
				{ field: 'zip_code', displayname: 'ZIP Code' },
				{ field: 'phone', displayname: 'Phone' },
				{ field: 'mob_phone', displayname: 'Mobile phone' },
				{ field: 'address', displayname: 'Address' },
				{
					name: 'Edit', width: 90, enableFiltering: false, enableCellEdit: false,
					cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.editAddress(row.entity)" value="Edit">'
				}
			],
			enableCellEditOnFocus: true,
			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {
				$scope.gridApi2 = gridApi;
				gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
					if (newValue != oldValue)
						$scope.saveAddress(rowEntity);
				});
			}


		};

		$scope.roleList = [];
		$scope.roleListSelected = [];
		$scope.userAddresses = [];


		$scope.deleteSelected = function () {

			if (!confirm($translate.instant('REMOVE_USER', { count : $scope.gridApi.selection.getSelectedCount()})))
				return;

			var selRows = $scope.gridApi.selection.getSelectedRows();

			if (selRows !== null) {
				angular.forEach(selRows, function (value, key) {
					index = $scope.users.indexOf(value);
					$scope.users.splice(index, 1);
					UserService.destroy(value.id);
					//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
					$scope.gridApi.selection.clearSelectedRows();
				});
			}
		}

		$scope.editUser = function (row) {
			$scope.currentUser = row;


			//Load roles & addresses
			UserService.show($scope.currentUser.id)
				.then(
				function (result) {
					$scope.roleListSelected = result.data.roles;

					$scope.userAddresses = result.data.addresses;
					$scope.addressGridOptions.data = $scope.userAddresses;

					return getOtherRoles(result.data.roles);
				}
				)
				.then(
				function (result) {
					$scope.roleList = result;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);



		}

		$scope.saveUser = function (user) {

			MS.showStatus('SAVING', 'info', 0);

			if (user.id) {
				//Updating
				UserService.update(user)
					.then(
					function (result) {
						MS.showStatus('SAVED', 'info');
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
			else {
				//Creating
				UserService.store(user)
					.then(
					function (result) {
						MS.showStatus('SAVED', 'info');
						$scope.users.push(result.data);
						$scope.currentUser = result.data;
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
		}

		$scope.userCreate = function () {
			$scope.currentUser = {};
			$scope.userDetails = null;

		}

		$scope.showUser = function (row) {

			$scope.userDetails = row;
			$scope.currentUser = null;
			$scope.newUser = null;

		}

		var getOtherRoles = function (roles) {

			var otherRoles = [];

			var deferred = $q.defer();

			UserService.indexRole()
				.then(function (result) {

					angular.forEach(result.data, function (value, key) {
						var found = false;

						angular.forEach(roles, function (v, k) {
							if (v.name == value.name)
								found = true;
						});

						if (!found) {
							otherRoles.push(value);
						}
					});

					deferred.resolve(otherRoles);

				},
				function (error) {
					deferred.reject(error);


				});
			return deferred.promise;

		}


		$scope.saveRoles = function () {

			UserService.clearRoles($scope.currentUser.id)
				.then(
				function (result) {

					angular.forEach($scope.roleListSelected, function (value, key) {
						return UserService.assignRole($scope.currentUser.id, value.id);

					});

					if ($scope.roleListSelected.length == 0)
						MS.showStatus('SAVED', 'info');

				}
				)
				.then(
				function () {
					MS.showStatus('SAVED', 'info');

				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);

		}


		//Addresses grid functions

		$scope.deleteSelectedAddresses = function () {

			if (!confirm($translate.instant('REMOVE_ADDRESS', { count : $scope.gridApi2.selection.getSelectedCount()})))
				return;

			var selRows = $scope.gridApi2.selection.getSelectedRows();

			if (selRows !== null) {
				angular.forEach(selRows, function (value, key) {
					UserService.destroyAddress(value.id)
						.then(
						function (result) {
							index = $scope.userAddresses.indexOf(value);
							$scope.userAddresses.splice(index, 1);

							//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
							$scope.gridApi2.selection.clearSelectedRows();
						},
						function (error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error');
						});

				});
			}
		}

		$scope.editAddress = function (row) {

			$scope.currentAddress = row;

		}

		$scope.addressCreate = function () {

			$scope.currentAddress = {};

		}

		$scope.saveAddress = function (address) {

			MS.showStatus('SAVING', 'info', -1);

			if (address.id) {
				//Updating
				UserService.updateAddress(address)
					.then(
					function (result) {
						MS.showStatus('SAVED');
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
			else {
				//Creating
				address.user_id = $scope.currentUser.id;
				UserService.storeAddress(address)
					.then(
					function (result) {
						$scope.userAddresses.push(result.data);
						MS.showStatus('SAVED');

					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
		}

	}]);