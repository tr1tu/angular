app.controller('TicketConfigController', ['$scope', 'TicketService', 'MessagesService', '$translate', 'uiGridConstants', 'ConfigService',
    function ($scope, TicketService, MS, $translate, uiGridConstants, ConfigService) {

        $scope.categoriesGridOptions = {

            enableFiltering: true,
            enableGridMenu: true,

            onRegisterApi: function (gridApi) {
                $scope.gridApiCategories = gridApi;

                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    if (newValue != oldValue) {
                        $scope.saveCategory(rowEntity);
                    }
                });
            }

        };

        $scope.prioritiesGridOptions = {

            enableFiltering: true,
            enableGridMenu: true,

            onRegisterApi: function (gridApi) {
                $scope.gridApiPriorities = gridApi;

                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    if (newValue != oldValue) {
                        $scope.savePriority(rowEntity);
                    }
                });
            }

        };


        $scope.translated = {};

        var requiredTranslations = [
            'SAVING', 'SAVED', 'NAME', 'LEVEL', 'GREATER_THAN', 'LESS_THAN'
        ];

        $translate(requiredTranslations)
            .then(
            function (translations) {
                $scope.translated.saving = translations.SAVING;
                $scope.translated.saved = translations.SAVED;
                $scope.translated.name = translations.NAME;
                $scope.translated.level = translations.LEVEL;
                $scope.translated.greater_than = translations.GREATER_THAN;
                $scope.translated.less_than = translations.LESS_THAN;

                $scope.categoriesGridOptions.columnDefs = [
                    { field: 'name', displayname: $scope.translated.name }

                ];

                $scope.prioritiesGridOptions.columnDefs = [
                    { field: 'name', displayname: $scope.translated.name },
                    {
                        field: 'level', displayname: $scope.translated.level,
                        filters: [
                            {
                                condition: uiGridConstants.filter.GREATER_THAN,
                                placeholder: $scope.translated.greater_than
                            },
                            {
                                condition: uiGridConstants.filter.LESS_THAN,
                                placeholder: $scope.translated.less_than
                            }
                        ]
                    }

                ];
            }
            );

        TicketService.indexTicketCategory()
            .then(
            function (result) {
                $scope.categories = result.data;
                $scope.categoriesGridOptions.data = $scope.categories;
            },
            function (error) {
                MS.showStatus(MS.getErrorsString(error.data), 'error');
            }
            );

        TicketService.indexTicketPriority()
            .then(
            function (result) {
                $scope.priorities = result.data;
                $scope.prioritiesGridOptions.data = $scope.priorities;
            },
            function (error) {
                MS.showStatus(MS.getErrorsString(error.data), 'error');
            }
            );

        ConfigService.index()
            .then(
            function (result) {
                $scope.config = result.data;
            }
            );

        $scope.saveConfig = function () {
            ConfigService.setMulti({ config: $scope.config })
                .then(
                function (result) {
                    MS.showStatus('SAVED', 'info');
                }
                , function (error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error');
                }
                )
        }

        $scope.saveCategory = function (category) {

            MS.showStatus($scope.translated.saving, 'info');

            if (category.id) {
                //Updating
                TicketService.updateTicketCategory(category)
                    .then(
                    function (result) {
                        MS.showStatus($scope.translated.saved);
                    },
                    function (error) {
                        MS.showStatus(MS.getErrorsString(error.data), 'error');
                    }
                    );
            }
            else {
                //Creating
                TicketService.storeTicketCategory(category)
                    .then(
                    function (result) {
                        category.id = result.data.id;
                        MS.showStatus($scope.translated.saved);

                    },
                    function (error) {
                        MS.showStatus(MS.getErrorsString(error.data), 'error');
                    }
                    );
            }


        }

        $scope.savePriority = function (priority) {

            MS.showStatus($scope.translated.saving, 'info');

            if (priority.id) {
                //Updating
                TicketService.updateTicketPriority(priority)
                    .then(
                    function (result) {
                        MS.showStatus($scope.translated.saved);
                    },
                    function (error) {
                        MS.showStatus(MS.getErrorsString(error.data), 'error');
                    }
                    );
            }
            else {
                //Creating
                TicketService.storeTicketPriority(priority)
                    .then(
                    function (result) {
                        priority.id = result.data.id;
                        MS.showStatus($scope.translated.saved);

                    },
                    function (error) {
                        MS.showStatus(MS.getErrorsString(error.data), 'error');
                    }
                    );
            }
        }

        $scope.categoryCreate = function () {
            var category = { name: "New category" };
            $scope.categories.push(category);

        }

        $scope.priorityCreate = function () {
            var priority = { name: "New priority", level: 0 };
            $scope.priorities.push(priority);

        }


        $scope.deleteSelectedCategories = function () {

            $translate('REMOVE_TICKET_CATEGORIES', { count: $scope.gridApiCategories.selection.getSelectedCount() })
                .then(
                function (translation) {

                    if (!confirm(translation))
                        return;


                    var selRows = $scope.gridApiCategories.selection.getSelectedRows();

                    if (selRows !== null) {
                        angular.forEach(selRows, function (value, key) {
                            TicketService.destroyTicketCategory(value.id)
                                .then(
                                function (result) {
                                    index = $scope.categories.indexOf(value);
                                    $scope.categories.splice(index, 1);
                                    //Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
                                    $scope.gridApiCategories.selection.clearSelectedRows();
                                },
                                function (error) {
                                    MS.showStatus(MS.getErrorsString(error.data), 'error');
                                }
                                );

                        });
                    }
                }
                );

        }

        $scope.deleteSelectedPriorities = function () {

            $translate('REMOVE_TICKET_PRIORITIES', { count: $scope.gridApiPriorities.selection.getSelectedCount() })
                .then(
                function (translation) {

                    if (!confirm(translation))
                        return;


                    var selRows = $scope.gridApiPriorities.selection.getSelectedRows();

                    if (selRows !== null) {
                        angular.forEach(selRows, function (value, key) {
                            TicketService.destroyTicketPriority(value.id)
                                .then(
                                function (result) {
                                    index = $scope.priorities.indexOf(value);
                                    $scope.priorities.splice(index, 1);
                                    //Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
                                    $scope.gridApiPriorities.selection.clearSelectedRows();
                                },
                                function (error) {
                                    MS.showStatus(MS.getErrorsString(error.data), 'error');
                                }
                                );

                        });
                    }
                }
                );

        }

    }]);