app.controller('DiscountController', ['$scope', 'ProductService', 'MessagesService', '$translate', 
	function($scope, ProductService, MS, $translate) {

	$scope.discountGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
       			{
	       				$scope.saveDiscount(rowEntity);
       			}
       		});
		}

	};

	$scope.translated = {};

	var requiredTranslations = [
		'NAME', 'TYPE', 'AMMOUNT', 'PERMANENT', 'START_DATE', 'END_DATE', 'SAVED', 'SAVING', 'EDIT', 'DATE_FORMAT', 
		'PLAIN', 'PERCENTAGE', 'YES', 'NO'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.type = translations.TYPE;
				$scope.translated.ammount = translations.AMMOUNT;
				$scope.translated.permanent = translations.PERMANENT;
				$scope.translated.start_date = translations.START_DATE;
				$scope.translated.end_date = translations.END_DATE;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.edit = translations.EDIT;
				$scope.translated.date_format = translations.DATE_FORMAT;
				$scope.translated.plain = translations.PLAIN;
				$scope.translated.percentage = translations.PERCENTAGE;
				$scope.translated.yes = translations.YES;
				$scope.translated.no = translations.NO;



				$scope.discountGridOptions.columnDefs = [
					{ field : 'name', name : $scope.translated.name },
					{
						field: 'type',
				        name: $scope.translated.type,
				        editType: 'dropdown',
				        enableCellEdit: true,
				        editableCellTemplate: 'ui-grid/dropdownEditor',
				        editDropdownOptionsArray: [ { ID : 0, label : $scope.translated.plain }, { ID : 1, label : $scope.translated.percentage } ],
				        editDropdownIdLabel: 'ID',
				        editDropdownValueLabel: 'label',
				        cellFilter: "griddropdown:editDropdownOptionsArray:editDropdownIdLabel:editDropdownValueLabel:row.entity.relatedobject.name"
			        },
					{ field : 'ammount', name : $scope.translated.ammount },
					{ 
						field : 'permanent',
						name : $scope.translated.permanent,
						editType: 'dropdown',
				        enableCellEdit: true,
				        editableCellTemplate: 'ui-grid/dropdownEditor',
				        editDropdownOptionsArray: [ { ID : 0, label : $scope.translated.no }, { ID : 1, label : $scope.translated.yes } ],
				        editDropdownIdLabel: 'ID',
				        editDropdownValueLabel: 'label',
				        cellFilter: "griddropdown:editDropdownOptionsArray:editDropdownIdLabel:editDropdownValueLabel:row.entity.relatedobject.name"
					},
					{ name : $scope.translated.edit,  width: 90, enableFiltering: false, enableCellEdit : false,
			cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.discountEdit(row.entity)" translate>EDIT</button>' }

				];
				

				$scope.options = [{ text : $scope.translated.plain, value : 0 }, { text : $scope.translated.percentage, value : 1 }];
				$scope.optionsPermanent = [{ text : $scope.translated.no, value : 0 }, { text : $scope.translated.yes, value : 1 }];
			}
		);


	$scope.discounts = [];

	ProductService.indexDiscount()
	.then(
			function(result) {	
				$scope.discounts = result.data;
				$scope.discountGridOptions.data = $scope.discounts;
				console.log($scope.discounts);
			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
		);


	$scope.discountCreate = function() {

		discount = { name : 'New discount' };
		$scope.discounts.push(discount);
		$scope.currentDiscount = discount;

	}


	$scope.deleteSelected = function() {

		$translate('REMOVE_DISCOUNT', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;


					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroyDiscount(value.id)
							.then(
									function(result) {
										index = $scope.discounts.indexOf(value);
										$scope.discounts.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error');
									}
								);
							
						});
					}
				}
			);

	}

	$scope.saveDiscount = function (discount) {

		MS.showStatus($scope.translated.saving, 'info');

		if(discount.id)
		{
			//Updating
			ProductService.updateDiscount(discount)
			.then(
					function(result) {
						MS.showStatus($scope.translated.saved);
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
				);
		}
		else
		{
			//Creating
			ProductService.storeDiscount(discount)
			.then(
					function(result) {
						discount.id = result.data.id;
						MS.showStatus($scope.translated.saved);

					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
				);
		}

	}

	$scope.discountEdit = function(discount) {

		$scope.currentDiscount = discount;
		$scope.start_date = discount.start_date;
		$scope.end_date = discount.end_date;

	}

}]);