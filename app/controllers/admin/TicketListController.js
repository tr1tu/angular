app.controller('TicketListController', ['$scope', 'TicketService', '$translate', 'MessagesService', '$state', 'uiGridConstants',
    function ($scope, TicketService, $translate, MS, $state, uiGridConstants) {

        $scope.ticketGridOptions = {

            enableFiltering: true,
            enableGridMenu: true,

            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
            },

        };

        $scope.translated = {};

        var requiredTranslations = [
            'CATEGORY', 'PRIORITY', 'SUBJECT', 'DATE', 'STATUS'
        ];

        $translate(requiredTranslations)
            .then(
            function (translations) {
                $scope.translated.category = translations.CATEGORY;
                $scope.translated.priority = translations.PRIORITY;
                $scope.translated.subject = translations.SUBJECT;
                $scope.translated.date = translations.DATE;
                $scope.translated.status = translations.STATUS;

                $scope.ticketGridOptions.columnDefs = [
                    { field: 'id', displayName: '#', sort : { direction : uiGridConstants.DESC }, type : 'number' },
                    { field: 'status.name', displayName: $scope.translated.status },
                    { field: 'subject', displayName: $scope.translated.subject },
                    { field: 'ticket_category.name', displayName: $scope.translated.category },
                    { field: 'ticket_priority.name', displayName: $scope.translated.priority },
                    {
                        name: 'details', displayName: '', enableFiltering: false, width: 80,
                        cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.showDetails(row.entity)" translate>DETAILS</button>'
                    }
                ];

            }
            );

        TicketService.index()
            .then(
            function (result) {
                $scope.tickets = result.data;
                $scope.ticketGridOptions.data = $scope.tickets;
            }
            );



        $scope.showDetails = function (ticket) {
            $state.go('ticketdetails', { id : ticket.id});
        }

    }]);