app.controller('TaxController', ['$scope', 'ProductService', 'MessagesService', '$translate', 
	function($scope, ProductService, MS, $translate) {


		$scope.taxGridOptions = {

			enableFiltering : true,
			enableGridMenu : true,

			onRegisterApi : function(gridApi) {
				$scope.gridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
	       			if(newValue != oldValue)
	       			{
		       				$scope.saveTax(rowEntity);
	       			}
	       		});
			}

		};


	$scope.translated = {};

	var requiredTranslations = [
		'NAME', 'TAX', 'SAVED', 'SAVING', 'NEW_TAX'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.tax = translations.TAX;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.new_tax = translations.NEW_TAX;

				$scope.taxGridOptions.columnDefs = [
					{ field : 'name', displayname : $scope.translated.name },
					{ field : 'tax', displayname : $scope.translated.tax }

				];

				
			}
		);


	$scope.taxes = [];


	ProductService.indexTax()
	.then(
			function(result) {
				$scope.taxes = result.data;
				$scope.taxGridOptions.data = $scope.taxes;
			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
		);


	$scope.saveTax = function(tax) {

		MS.showStatus($scope.translated.saving, 'info');

    		if(tax.id)
    		{
    			//Updating
    			ProductService.updateTax(tax)
    			.then(
    					function(result) {
    						MS.showStatus($scope.translated.saved);
    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error');
    					}
    				);
    		}
    		else
    		{
    			//Creating
    			ProductService.storeTax(tax)
    			.then(
    					function(result) {
    						tax.id = result.data.id;
    						MS.showStatus($scope.translated.saved);

    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error');
    					}
    				);
    		}


	}


	$scope.deleteSelected = function() {

		$translate('REMOVE_TAX', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;


					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroyTax(value.id)
							.then(
									function(result) {
										index = $scope.taxes.indexOf(value);
										$scope.taxes.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error');
									}
								);
							
						});
					}
				}
			);

	}

	$scope.taxCreate = function() {
		var tax = { name : $scope.translated.new_tax };
		$scope.taxes.push(tax);

	}


}]);