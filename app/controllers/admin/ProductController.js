app.controller('ProductController', ['$scope', 'ProductService', 'MessagesService', '$translate', '$filter', 'FileService', 'CustomModalService',
	function($scope, ProductService, MS, $translate, $filter, FileService, CMService){

	//Variable to hide or show progressbar
	$scope.uploading = false;
	$scope.uploadingColorImage = false;

	$scope.productGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {

			$scope.gridApi = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
	       				$scope.updateProduct(rowEntity, colDef, oldValue);
       		});
		},

	};

	$scope.colorGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {
			$scope.gridApi2 = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
       			{
	       				$scope.saveColor(rowEntity);
       			}
       		});
		}

	};

	$scope.colorPickerOptions = {
		format : 'hex'
	};

	$scope.translated = {};

	var requiredTranslations = [
		'NAME', 'PRICE', 'DESCRIPTION', 'REFERENCE', 'STOCK', 'SAVED', 'SAVING', 'EDIT', 'IMAGE', 'RGB'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.price = translations.PRICE;
				$scope.translated.description = translations.DESCRIPTION;
				$scope.translated.reference = translations.REFERENCE;
				$scope.translated.stock = translations.STOCK;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.edit = translations.EDIT;
				$scope.translated.image = translations.IMAGE;
				$scope.translated.rgb = translations.RGB;

				$scope.productGridOptions.columnDefs = [
					{ field : 'name', name : $scope.translated.name },
					{ field : 'price', name : $scope.translated.price },
					{ field : 'description', name : $scope.translated.description },
					{ field : 'reference', name : $scope.translated.reference },
					/*{ field : 'stock', name : $scope.translated.stock },*/
					{ name : $scope.translated.edit, width: 60, enableFiltering: false, enableCellEdit : false,
			cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.productEdit(row.entity)" translate>EDIT</button>' }

				];

				$scope.colorGridOptions.columnDefs = [
					{ field : 'name' , displayname : $scope.translated.name },
					{ name : 'RGB', displayname : $scope.translated.rgb, width: 75, enableFiltering : false, enableCellEdit: false,
					cellTemplate:"<div style=\"height:20px;width:20px;margin: auto;background-color: {{grid.getCellValue(row, col)}}\">" },
					{ name : $scope.translated.edit,  width: 90, enableFiltering: false, enableCellEdit: false,
			cellTemplate: '<button class="btn btn-primary btn-block" type="button" ng-click="grid.appScope.colorEdit(row.entity)" translate>EDIT</button>' }
				];

			}
		);


	ProductService.index()
    	.then(function(result) {

    		$scope.products = result.data;
    		$scope.productGridOptions.data = $scope.products;

	});


    //////////////////////////////////////////
    /////////////LOAD SELECTS////////////////
    ////////////////////////////////////////

    //Sizing select
    ProductService.indexSizing()
    .then(
    		function(result) {
    			$scope.sizingList = result.data;
    		},
    		function(error) {
    			MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
    		}
    	);

    //Tax select
    ProductService.indexTax()
    .then(
    		function(result) {
    			$scope.taxList = result.data;
    		},
    		function(error) {
    			MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
    		}
    	);

    //Discount select
    ProductService.indexDiscount()
    .then(
    		function(result) {
    			$scope.discountList = result.data;
    		},
    		function(error) {
    			MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
    		}
    	);

    //Categories select
    ProductService.indexCategory()
    .then(
    		function(result) {
    			$scope.categoryList = result.data;
    		},
    		function(error) {
    			MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
    		}
    	);


	$scope.updateProduct = function(product, colDef, oldValue) {
		colDef = colDef || null;
		oldValue = oldValue || null;

		MS.showStatus($scope.translated.saving, 'info', -1);

		if(product.id)
		{
			//Update
			ProductService.update(product)
			.then(
					function(result) {
						MS.showStatus($scope.translated.saved);
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
					}
				);
		}
		else
		{
			//Store
			ProductService.store(product)
			.then(
					function(result) {
						$scope.products.push(result.data);
						$scope.currentProduct = result.data;
						MS.showStatus($scope.translated.saved);
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
					}
				);
		}

	};

	$scope.productCreate = function() {

		$scope.currentProduct = {
			sizings : [],
			taxes : [],
			discounts : [],
			category : {}
		};


	}

	$scope.productEdit = function(row) {
		ProductService.show(row.id)
		.then(
				function(result) {
					row.sizings = result.data.sizings;
					row.taxes = result.data.taxes;
					row.discounts = result.data.discounts;
					row.colors = result.data.colors;
					row.images = result.data.images;
					row.tagged = result.data.tagged;
					row.category = result.data.category;
					$scope.currentProduct = row;
					$scope.colorGridOptions.data = $scope.currentProduct.colors;
					$scope.currentImage = null;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 7000);
				}
			);
		
	}

	$scope.deleteSelected = function() {

		$translate('REMOVE_PRODUCTS', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;

					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroy(value.id)
							.then(
									function(result) {
										index = $scope.products.indexOf(value);
										$scope.products.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
									}
								);
							
						});
					}
				}
			);	

	}

	$scope.colorEdit = function(row) {
		$scope.currentColor = row;
	}

	$scope.saveColor = function(color) {

			MS.showStatus($scope.translated.saving, 'info', -1);

    		if(color.id)
    		{
    			//Updating
    			ProductService.updateColor(color)
    			.then(
    					function(result) {
    						$scope.currentColor = null;
    						MS.showStatus($scope.translated.saved);
    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}
    		else
    		{
    			//Creating
    			color.product_id = $scope.currentProduct.id;
    			ProductService.storeColor(color)
    			.then(
    					function(result) {
    						$scope.currentColor = null;
    						MS.showStatus($scope.translated.saved);

    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}
	}


	$scope.deleteSelectedColor = function() {

		if(!confirm('Are you sure you want to remove ' + $scope.gridApi2.selection.getSelectedCount() + ' colors?'))
				return;

		var selRows = $scope.gridApi2.selection.getSelectedRows();

		if(selRows !== null) {
			angular.forEach(selRows, function(value, key) {
				index = $scope.currentProduct.colors.indexOf(value);
				$scope.currentProduct.colors.splice(index, 1);
				ProductService.destroyColor(value.id);
				//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
				$scope.gridApi2.selection.clearSelectedRows();
			});
		}

	}

	$scope.colorCreate = function() {
		color = { name : 'New color' };
		$scope.currentProduct.colors.push(color);
		$scope.currentColor = color;
	}


	$scope.deleteSelectedImage = function() {

		$translate('REMOVE_IMAGES', { count :  $scope.selectedImages.length })
		.then(
				function(translation) {


					if(!confirm(translation))
						return;



					angular.forEach($scope.selectedImages, function(value, key) {

						ProductService.destroyImage(value.id)
						.then(
								function(result) {
									index = $scope.currentProduct.images.indexOf(value);
									$scope.currentProduct.images.splice(index, 1);
								},
								function(error) {
									MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
								}
							);
						
					});
					
				}
			);

	}

	$scope.openNewImage = function() {
		$scope.newImage = {};
	}

	$scope.addImage = function() {
		$scope.currentProduct.images.push($scope.newImage);
		$scope.saveAllImages();
	}

	$scope.saveImage = function(image) {

		MS.showStatus($scope.translated.saving, 'info');

    		if(image.id)
    		{
    			//Updating
    			ProductService.updateImage(image)
    			.then(
    					function(result) {
    						//$scope.currentImage = null;
    						MS.showStatus($scope.translated.saved);
    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error');
    					}
    				);
    		}
    		else
    		{
    			//Creating
    			image.product_id = $scope.currentProduct.id;
    			ProductService.storeImage(image)
    			.then(
    					function(result) {
    						//$scope.currentImage = null;
    						image.id = result.data.id;
    						image.order = result.data.order;
    						//Ya no es necesario añadirlos porque ya estaran añadidos cuando guardemos.
    						//$scope.currentProduct.images.push(image);
    						MS.showStatus($scope.translated.saved);

    					},
    					function(error) {
    						MS.showStatus(MS.getErrorsString(error.data), 'error');
    					}
    				);
    		}

	}

	$scope.saveAllImages = function() {
		
		//We must set every order to null before saving. Otherwise we will have problems with unique order.
		ProductService.clearImageOrder($scope.currentProduct.id)
		.then(
				function(result) {
					var order = 1;
					angular.forEach($scope.currentProduct.images, function(v,k) {
						v.order = order++;
						$scope.saveImage(v);
					});
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
			);

		
	}

	$scope.addTag = function(tag) {

		if(!tag || tag == "")
			return;

		var found = false;
		angular.forEach($scope.currentProduct.tagged, function(v,k) {
			if(v.tag_name == tag)
				found = true;
		});
		if(!found)
			$scope.currentProduct.tagged.push({ tag_name : tag });
	}

	$scope.deleteTags = function() {
		angular.forEach($scope.selectedTags, function(value, key) {
	    		var found = $filter('filter')($scope.currentProduct.tagged, {tag_name: value}, true);
			     if (found.length) {
			         $scope.currentProduct.tagged.splice($scope.currentProduct.tagged.indexOf(found[0]), 1);
			     }
		 	});

	}

	$scope.saveTags = function() {

		var tags = [];

		angular.forEach($scope.currentProduct.tagged, function(v,k) {
			tags.push(v.tag_name);
		});

		ProductService.setTags($scope.currentProduct.id, tags)
		.then(
				function(result) {
					MS.showStatus($scope.translated.saved);
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);

	}

    /*$scope.uploadImages = function (files) {
      if (files && files.length) {
      	$scope.uploading = true;
        FileService.upload(files, 'public')
        .then(
        	function (resp) {

        		FileService.saveResizedImages(resp.data)
        		.then(
        				function(result) {

        					angular.forEach(resp.data, function(v,k) {
				            	$scope.currentProduct.images.push({ description : v, url : v, small_url : result.data[k] });
				            });

				            $scope.saveAllImages();	
	          				$scope.uploading = false;
	            			$scope.imagesProgress = 0;

        				},
        				function(error) {
        					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
        					$scope.uploading = false;
	            			$scope.imagesProgress = 0;
        				}
        			);
	            
	            
	        }, 
	        function (error) {
	            MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
	            $scope.uploading = false;
	            $scope.imagesProgress = 0;
	        }, 
	        function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            $scope.imagesProgress = progressPercentage;
	        }
        );
      }
    }*/

    /*$scope.uploadColorImage = function (files) {
      if (files && files.length) {
      	$scope.uploadingColorImage = true;
        FileService.upload(files, 'public')
        .then(
        	function (resp) {
            	$scope.currentColor.image = resp.data['0'];    
	            $scope.uploadingColorImage = false;
	            $scope.imageColorProgress = 0;
	        }, 
	        function (error) {
	            MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
	            $scope.uploadingColorImage = false;
	            $scope.imageColorProgress = 0;
	        }, 
	        function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            $scope.imageColorProgress = progressPercentage;
	        }
        );
      }
    }*/


	/*************/
	/** MODALS **/
	/***********/

	$scope.previewModal = {};
	$scope.uploadModal = {};

	$scope.openPreviewModal = function(field) {
		$scope.previewModal = CMService.imagePreview(field).result.then(function(result) {}, function(result) {});
	}

	$scope.openUploadModal = function(options, fieldname) {
		$scope.uploadModal = CMService.uploadImage().result.then(function(result) {
			options[fieldname] = result;
		}, function(result) {});
	}

}]);