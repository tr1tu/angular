app.controller('SizingController', ['$scope', 'ProductService', 'MessagesService', '$translate', 
	function($scope, ProductService, MS, $translate) {


	$scope.sizingGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
       			{
	       				$scope.saveSizing(rowEntity, colDef, oldValue);
       			}
       		});
		}

	};

	$scope.sizeGridOptions = {

		enableFiltering : true,
		enableGridMenu : true,

		onRegisterApi : function(gridApi) {
			$scope.gridApi2 = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
       			if(newValue != oldValue)
       			{
	       				$scope.saveSize(rowEntity);
       			}
       		});
		}

	};


	$scope.translated = {};

	var requiredTranslations = [
		'NAME', 'SIZING_NUMBER', 'SAVED', 'SAVING', 'EDIT', 'EDIT_SIZES', 'SIZE', 'MEASURE'
	];

	$translate(requiredTranslations)
	.then(
			function(translations) {
				$scope.translated.name = translations.NAME;
				$scope.translated.sizing_number = translations.SIZING_NUMBER;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.saving = translations.SAVING;
				$scope.translated.edit = translations.EDIT;
				$scope.translated.edit_sizes = translations.EDIT_SIZES;
				$scope.translated.size = translations.SIZE;
				$scope.translated.measure = translations.MEASURE;

				$scope.sizingGridOptions.columnDefs = [
					{ field : 'name', name : $scope.translated.name },
					{ field : 'sizing_number', name : $scope.translated.sizing_number },
					{ name : $scope.translated.edit_sizes,  width: 120, enableFiltering: false, enableCellEdit : false,
			cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.sizesEdit(row.entity)" value="' + $scope.translated.edit_sizes + '" />' }

				];

				$scope.sizeGridOptions.columnDefs = [
					{ field : 'size', name : $scope.translated.size },
					{ field : 'measure', name : $scope.translated.measure }

				];

				
			}
		);

	$scope.sizings = [];

	ProductService.indexSizing()
	.then(
			function(result) {
				$scope.sizings = result.data;
				$scope.sizingGridOptions.data = $scope.sizings;

			},
			function(error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error', 6000);
			}
		);



	$scope.saveSizing = function(sizing, colDef, oldValue) {
		colDef = colDef || null;
		oldValue = oldValue || null;

		MS.showStatus($scope.translated.saving, 'info', -1);

    		if(sizing.id)
    		{
    			//Updating
    			ProductService.updateSizing(sizing)
    			.then(
    					function(result) {
    						MS.showStatus($scope.translated.saved);
    					},
    					function(error) {
    						//if(colDef != null && oldValue != null) sizing[colDef.name] = oldValue;
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}
    		else
    		{
    			//Creating
    			ProductService.storeSizing(sizing)
    			.then(
    					function(result) {
    						sizing.id = result.data.id;
    						MS.showStatus($scope.translated.saved);

    					},
    					function(error) {
    						//if(colDef != null && oldValue != null) sizing[colDef.name] = oldValue;
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}


	}

	$scope.sizesEdit = function(sizing) {

		$scope.currentSizing = sizing;

		ProductService.showSizing(sizing.id)
		.then(
				function(result) {
					$scope.currentSizing.sizes = result.data.sizes;
					$scope.sizeGridOptions.data = $scope.currentSizing.sizes;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 6000);
				}
			);

	}

	$scope.deleteSelected = function() {

		$translate('REMOVE_SIZING', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;


					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroySizing(value.id)
							.then(
									function(result) {
										index = $scope.sizings.indexOf(value);
										$scope.sizings.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
									}
								);
							
						});
					}
				}
			);	

	}

	$scope.sizingCreate = function() {

		var sizing = { name : "New sizing" };
		$scope.sizings.push(sizing);

	}


	$scope.saveSize = function(size) {

		MS.showStatus($scope.translated.saving, 'info', -1);

    		if(size.id)
    		{
    			//Updating
    			ProductService.updateSize(size)
    			.then(
    					function(result) {
    						MS.showStatus($scope.translated.saved);
    					},
    					function(error) {
    						//if(colDef != null && oldValue != null) size[colDef.name] = oldValue;
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}
    		else
    		{
    			//Creating
    			size.sizing_id = $scope.currentSizing.id;
    			ProductService.storeSize(size)
    			.then(
    					function(result) {
    						size.id = result.data.id;
    						MS.showStatus($scope.translated.saved);

    					},
    					function(error) {
    						//if(colDef != null && oldValue != null) size[colDef.name] = oldValue;
    						MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
    					}
    				);
    		}

	}

	$scope.sizeCreate = function() {

		var size = { size : 0 };
		$scope.currentSizing.sizes.push(size);

	}

	$scope.deleteSelectedSizes = function() {

		$translate('REMOVE_SIZE', { count :  $scope.gridApi2.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;


					var selRows = $scope.gridApi2.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroySize(value.id)
							.then(
									function(result) {
										index = $scope.currentSizing.sizes.indexOf(value);
										$scope.currentSizing.sizes.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi2.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
									}
								);
							
						});
					}
				}
			);

	}

}]);