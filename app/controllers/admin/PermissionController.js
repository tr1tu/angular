app.controller('PermissionController', ['$scope', 'UserService', 'API_URL', '$state', '$q', 'MessagesService',
	function ($scope, UserService, apiUrl, $state, $q, MS) {

		$scope.permissionGridOptions = {};
		$scope.permissionGridOptions.columnDefs = [
			{ field: 'name', displayname: 'Role name' },
			{ field: 'display_name', displayname: 'Display name' },
			{ field: 'description', displayname: 'Description' },
			{ name: 'Edit', width: 90, cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.editPermission(row.entity)" value="Edit" />' }
		];

		//$scope.permissionGridOptions.enableFullRowSelection = true;
		//$scope.permissionGridOptions.modifierKeysToMultiSelect = true;

		$scope.permissionGridOptions.enableFiltering = true;
		$scope.permissionGridOptions.enableGridMenu = true;

		$scope.permissionGridOptions.onRegisterApi = function (gridApi) {
			$scope.gridApi = gridApi;

			gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
				if (newValue != oldValue)
					$scope.updatePermission(rowEntity, colDef, oldValue);
			});
		};

		UserService.indexPermission()
			.then(function (result) {

				$scope.permissions = result.data;
				$scope.permissionGridOptions.data = $scope.permissions;

			});

		$scope.updatePermission = function (permission, colDef, oldValue) {
			colDef = colDef || null;
			oldValue = oldValue || null;

			UserService.showPermission(permission.id)
				.then(function (result) {
					//Estamos actualizando un permiso ya existente.
					if (result.data) {
						UserService.updatePermission(permission)
							.then(
							function (result) {
								MS.showStatus('Saved!');
							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
								if (colDef != null && oldValue != null) permission[colDef.name] = oldValue;
							});
					}
					else	//Estamos añadiendo un nuevo permiso
					{
						UserService.storePermission(permission)
							.then(
							function (result) {
								permission.id = result.data.id;

								MS.showStatus('Saved!');

							},
							function (error) {
								MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
							});
					}
				});

		};



		$scope.permissionCreate = function () {

			permission = { name: 'New unsaved permission' };
			$scope.permissions.push(permission);

		};

		$scope.deleteSelected = function () {

			if (!confirm('Are you sure you want to remove ' + $scope.gridApi.selection.getSelectedCount() + ' permissions?'))
				return;

			var selRows = $scope.gridApi.selection.getSelectedRows();

			angular.forEach(selRows, function (value, key) {
				index = $scope.permissions.indexOf(value);
				$scope.permissions.splice(index, 1);
				UserService.destroyPermission(value.id);
				$scope.gridApi.selection.clearSelectedRows();
			});

		};


		$scope.editPermission = function (perm) {
			$scope.editingPerm = perm;

		}


	}]);