app.controller('CommonAdminController', ['$scope', 'MessagesService', 'AuthService', 'ConfigService',
	function ($scope, MS, AuthService, ConfigService) {
		$scope.ms = MS;
		$scope.auth = AuthService;

		/////////////////////
		////CONFIG DATA/////
		///////////////////

		ConfigService.getCompanyData()
			.then(
			function (result) {
				$scope.companyData = result.data;
			}
			);


		ConfigService.getSocialMedia()
			.then(
			function (result) {
				$scope.socialMedia = result.data;
			}
			);
	}]);