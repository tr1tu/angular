app.controller('ShipmentController', ['$scope', 'OrderService', 'MessagesService', '$translate', 'FileService',
	function($scope, OrderService, MS, $translate, FileService){


		$scope.shipmentGridOptions = {

			enableFiltering : true,
			enableGridMenu : true,

			onRegisterApi : function(gridApi) {

				$scope.gridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
	       			if(newValue != oldValue)
		       				$scope.updateShipment(rowEntity);
	       		});
			},

		};


		$scope.translated = {};

		var requiredTranslations = [
			'NAME', 'FEE', 'SAVED', 'SAVING', 'EDIT'
		];

		$translate(requiredTranslations)
		.then(
				function(translations) {
					$scope.translated.name = translations.NAME;
					$scope.translated.fee = translations.FEE;
					$scope.translated.saved = translations.SAVED;
					$scope.translated.saving = translations.SAVING;
					$scope.translated.edit = translations.EDIT;
					

					$scope.shipmentGridOptions.columnDefs = [
						{ field : 'name', name : $scope.translated.name },
						{ field : 'fee', name : $scope.translated.fee },
						{ name : $scope.translated.edit,  width: 90, enableFiltering: false, enableCellEdit : false,
				cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.shipmentEdit(row.entity)" value="' + $scope.translated.edit + '" />' }

					];

				}
			);


		OrderService.indexShipment()
		.then(
				function(result) {
					$scope.shipments = result.data;
					$scope.shipmentGridOptions.data = $scope.shipments;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);


		$scope.updateShipment = function(shipment) {

			if(shipment.id)
			{
				//update
				OrderService.updateShipment(shipment)
				.then(
						function(result) {
							MS.showStatus($scope.translated.saved);
						},
						function(error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error');
						}
					);
			}
			else
			{
				//store
				OrderService.storeShipment(shipment)
				.then(
						function(result) {
							shipment.id = result.data.id;
							MS.showStatus($scope.translated.saved);
						},
						function(error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error');
						}
					);
			}

		}


		$scope.shipmentEdit = function(shipment) {
			$scope.currentShipment = shipment;
		}


		$scope.uploadImage = function (files) {
	      if (files && files.length) {
	        FileService.upload(files, 'public')
	        .then(
	        	function (resp) {
	            	$scope.currentShipment.image = resp.data['0'];    
		        }, 
		        function (error) {
		            MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
		        }
	        );
	      }
   		}

   		$scope.deleteSelected = function() {

		$translate('REMOVE_SHIPMENTS', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;

					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							OrderService.destroyShipment(value.id)
							.then(
									function(result) {
										index = $scope.shipments.indexOf(value);
										$scope.shipments.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
									}
								);
							
						});
					}
				}
			);	

	}

	$scope.shipmentCreate = function() {
		var shipment = {};
		$scope.shipments.push(shipment);
		$scope.currentShipment = shipment;
	}


}]);