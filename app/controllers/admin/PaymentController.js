app.controller('PaymentController', ['$scope', 'OrderService', 'MessagesService', '$translate', 'FileService',
	function($scope, OrderService, MS, $translate, FileService){


		$scope.paymentGridOptions = {

			enableFiltering : true,
			enableGridMenu : true,

			onRegisterApi : function(gridApi) {

				$scope.gridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
	       			if(newValue != oldValue)
		       				$scope.updatePayment(rowEntity);
	       		});
			},

		};


		$scope.translated = {};

		var requiredTranslations = [
			'NAME', 'FEE', 'SAVED', 'SAVING', 'EDIT'
		];

		$translate(requiredTranslations)
		.then(
				function(translations) {
					$scope.translated.name = translations.NAME;
					$scope.translated.fee = translations.FEE;
					$scope.translated.saved = translations.SAVED;
					$scope.translated.saving = translations.SAVING;
					$scope.translated.edit = translations.EDIT;
					

					$scope.paymentGridOptions.columnDefs = [
						{ field : 'name', name : $scope.translated.name },
						{ field : 'fee', name : $scope.translated.fee },
						{ name : $scope.translated.edit,  width: 90, enableFiltering: false, enableCellEdit : false,
				cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.paymentEdit(row.entity)" value="' + $scope.translated.edit + '" />' }

					];

				}
			);


		OrderService.indexPayment()
		.then(
				function(result) {
					$scope.payments = result.data;
					$scope.paymentGridOptions.data = $scope.payments;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);


		$scope.updatePayment = function(payment) {

			if(payment.id)
			{
				//update
				OrderService.updatePayment(payment)
				.then(
						function(result) {
							MS.showStatus($scope.translated.saved);
						},
						function(error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
						}
					);
			}
			else
			{
				//store
				OrderService.storePayment(payment)
				.then(
						function(result) {
							payment.id = result.data.id;
							MS.showStatus($scope.translated.saved);
						},
						function(error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
						}
					);
			}

		}


		$scope.paymentEdit = function(payment) {
			$scope.currentPayment = payment;
		}


		$scope.uploadImage = function (files) {
	      if (files && files.length) {
	        FileService.upload(files, 'public')
	        .then(
	        	function (resp) {
	            	$scope.currentPayment.image = resp.data['0'];    
		        }, 
		        function (error) {
		            MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
		        }
	        );
	      }
   		}


   		$scope.deleteSelected = function() {

		$translate('REMOVE_PAYMENTS', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;

					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							OrderService.destroyPayment(value.id)
							.then(
									function(result) {
										index = $scope.payments.indexOf(value);
										$scope.payments.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
									}
								);
							
						});
					}
				}
			);	

	}

	$scope.paymentCreate = function() {
		var payment = {};
		$scope.payments.push(payment);
		$scope.currentPayment = payment;
	}


}]);