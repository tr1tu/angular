app.controller('TicketController', ['$scope', '$stateParams', 'TicketService', 'MessagesService', '$state', '$translate',
    function ($scope, $stateParams, TicketService, MS, $state, $translate) {

        var ticketId = $stateParams.id;

        $scope.admin = true;

        $scope.translated = {};

        var requiredTranslations = [
            'TICKET_DELETED', 'CONFIRM_DELETE_TICKET'
        ];

        $translate(requiredTranslations)
            .then(
            function (translations) {
                $scope.translated.ticket_deleted = translations.TICKET_DELETED;
                $scope.translated.confirm_delete_ticket = translations.CONFIRM_DELETE_TICKET;
            }
            );

        var load = function () {
            TicketService.show(ticketId)
                .then(
                function (result) {
                    $scope.ticket = result.data;
                },
                function (error) {
                    $state.go('ticketlist');
                }
                );
        }

        load();

        $scope.openReply = function (opened) {
            opened = opened != null ? opened : true;
            $scope.replyOpened = opened;
        }

        $scope.sendReply = function (message) {
            if (message) {
                var ticketAnswer = {
                    message: message,
                    ticket_id: ticketId
                };
                console.log('test');
                TicketService.storeTicketAnswer(ticketAnswer)
                    .then(
                    function (result) {
                        $scope.message = "";
                        $scope.openReply(false);
                        load();
                    },
                    function (error) {
                        MS.showStatus(MS.getErrorsString(error.data), 'error');
                    }
                    );
            }
        }

        $scope.deleteTicket = function (ticket) {

            if (!confirm($scope.translated.confirm_delete_ticket))
                return;

            TicketService.destroy(ticket.id)
                .then(
                function (result) {
                    $state.go('ticketlist');
                    MS.showStatus($scope.translated.ticket_deleted, 'info', -1);
                },
                function (error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
                }
                );
        }

        $scope.assignTicket = function (ticket, userId) {
            t = {
                id: ticket.id,
                agent_id: userId
            };
            TicketService.update(t)
                .then(
                function (result) {
                    load();
                },
                function (error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error');
                }
                );
        }

        $scope.changeStatus = function (status) {
            $scope.ticket.status = status;
            TicketService.update($scope.ticket)
                .then(
                function (result) {
                    load();
                },
                function (error) {
                    MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
                }
                );
        }

    }])