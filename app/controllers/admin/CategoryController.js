app.controller('CategoryController', ['$scope', 'ProductService', 'MessagesService', '$translate', 
	function($scope, ProductService, MS, $translate) {


		$scope.categoryGridOptions = {

			enableFiltering : true,
			enableGridMenu : true,

			onRegisterApi : function(gridApi) {
				$scope.gridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
	       			if(newValue != oldValue)
	       			{
		       				$scope.saveCategory(rowEntity);
	       			}
	       		});
			}

		};


		$scope.translated = {};

		var requiredTranslations = [
			'NAME', 'DESCRIPTION', 'PARENT', 'ACTIVE', 'SAVED', 'SAVING', 'EDIT', 'YES', 'NO'
		];

		$translate(requiredTranslations)
		.then(
				function(translations) {
					$scope.translated.name = translations.NAME;
					$scope.translated.description = translations.DESCRIPTION;
					$scope.translated.parent = translations.PARENT;
					$scope.translated.active = translations.ACTIVE;
					$scope.translated.saved = translations.SAVED;
					$scope.translated.saving = translations.SAVING;
					$scope.translated.edit = translations.EDIT;
					$scope.translated.yes = translations.YES;
					$scope.translated.no = translations.NO;



					$scope.categoryGridOptions.columnDefs = [
						{ field : 'name', displayName : $scope.translated.name },
						{ field : 'description', displayName : $scope.translated.description },
						{ field : 'parent.name', displayName : $scope.translated.parent },
						{ 
							field : 'active',
							displayName : $scope.translated.active,
							editType: 'dropdown',
					        enableCellEdit: true,
					        editableCellTemplate: 'ui-grid/dropdownEditor',
					        editDropdownOptionsArray: [ { ID : 0, label : $scope.translated.no }, { ID : 1, label : $scope.translated.yes } ],
					        editDropdownIdLabel: 'ID',
					        editDropdownValueLabel: 'label',
					        cellFilter: "griddropdown:editDropdownOptionsArray:editDropdownIdLabel:editDropdownValueLabel:row.entity.relatedobject.name"
						},
						{ name : 'edit', displayName : $scope.translated.edit,  width: 90, enableFiltering: false, enableCellEdit : false,
				cellTemplate: '<input class="btn btn-primary" type="button" ng-click="grid.appScope.categoryEdit(row.entity)" value="' + $scope.translated.edit + '" />' }

					];


					$scope.optionsActive = [{ text : $scope.translated.no, value : 0 }, { text : $scope.translated.yes, value : 1 }];
				}
			);


		$scope.categories = [];

		ProductService.indexCategory()
		.then(
				function(result) {	
					$scope.categories = result.data;
					$scope.categoryGridOptions.data = $scope.categories;
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 6000);
				}
			);


		$scope.categoryEdit = function(category) {
			
			ProductService.showCategory(category.id)
			.then(
					function(result) {
						category.tagged = [];
						angular.forEach(result.data.tagged, function(v,k) {
							category.tagged.push(v.tag_name);							
						});
						$scope.currentCategory = category;
					},
					function(error) {

					}
				);
		}

		$scope.categoryCreate = function() {

			category = { name : 'New category' };
			$scope.categories.push(category);
			$scope.currentCategory = category;

		}

		$scope.deleteSelected = function() {

		$translate('REMOVE_CATEGORIES', { count :  $scope.gridApi.selection.getSelectedCount() })
		.then(
				function(translation) {

					if(!confirm(translation))
						return;


					var selRows = $scope.gridApi.selection.getSelectedRows();

					if(selRows !== null) {
						angular.forEach(selRows, function(value, key) {
							ProductService.destroyCategory(value.id)
							.then(
									function(result) {
										index = $scope.categories.indexOf(value);
										$scope.categories.splice(index, 1);
										//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
										$scope.gridApi.selection.clearSelectedRows();
									},
									function(error) {
										MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
									}
								);
							
						});
					}
				}
			);

	}

	$scope.saveCategory = function (category) {

		MS.showStatus($scope.translated.saving, 'info');

		category.parent_id = category.parent != null ? category.parent.id : -1;

		if(category.id)
		{
			//Updating
			ProductService.updateCategory(category)
			.then(
					function(result) {
						MS.showStatus($scope.translated.saved);
					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
				);
		}
		else
		{
			//Creating
			ProductService.storeCategory(category)
			.then(
					function(result) {
						category.id = result.data.id;
						MS.showStatus($scope.translated.saved);

					},
					function(error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
				);
		}

	}

	$scope.deselectParent = function() {
		$scope.currentCategory.parent = null;
	}

	$scope.addTag = function(tag) {
		var found = false;
		angular.forEach($scope.currentCategory.tagged, function(v,k) {
			if(v.tag_name == tag)
				found = true;
		});
		if(!found)
			$scope.currentCategory.tagged.push({ tag_name : tag });
	}

	$scope.deleteTags = function() {
		angular.forEach($scope.selectedTags, function(value, key) {
	    		var found = $filter('filter')($scope.currentCategory.tagged, {tag_name: value}, true);
			     if (found.length) {
			         $scope.currentCategory.tagged.splice($scope.currentCategory.tagged.indexOf(found[0]), 1);
			     }
		 	});

	}

	$scope.saveTags = function() {

		var tags = [];

		angular.forEach($scope.currentCategory.tagged, function(v,k) {
			tags.push(v);
		});

		ProductService.setTagsCat($scope.currentCategory.id, tags)
		.then(
				function(result) {
					MS.showStatus($scope.translated.saved);
				},
				function(error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
			);

	}

}]);