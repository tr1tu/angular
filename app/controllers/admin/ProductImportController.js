app.controller('ProductImportController', ['$scope', 'ProductService', 'MessagesService', '$translate', '$filter', 'FileService', 'uiGridConstants',
	function ($scope, ProductService, MS, $translate, $filter, FileService, uiGridConstants) {

		//Import settings object.
		$scope.importSettings = {};

		$scope.importGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {

				$scope.gridApi = gridApi;

			},

		};


		$scope.variablesGridOptions = {

			enableFiltering: true,
			enableGridMenu: true,

			onRegisterApi: function (gridApi) {
				$scope.variablesGridApi = gridApi;

				gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
					if (newValue != oldValue && rowEntity.name && rowEntity.name.length) {

						var found = 0;

						angular.forEach($scope.importVariables, function(setting) {
							if(setting.name == rowEntity.name)
								found++;
						});

						if(found >= 2)
							MS.showStatus('REPEATED_VARIABLE', 'warning');
						else
							$scope.saveVariable(rowEntity);
					}
				});
			}

		};

		$scope.translated = {};

		var requiredTranslations = [
			'IMPORT_SUCCESS', 'CODE', 'NAME', 'EXISTS', 'CATEGORY', 'DISCONTINUED', 'DESCRIPTION', 'PRICE',
			'STOCK', 'REFERENCE', 'EAN', 'TRADING_PRICE', 'UNITS_PER_PACK', 'MANUFACTURER', 'IMAGE_URL', 'SMALL_IMAGE_URL',
			'YES', 'NO', 'LESS_THAN', 'GREATER_THAN', 'SAVED', 'LOAD_PROFILE_CONFIRM', 'STARTS_WITH', 'NOT_STARTS_WITH',
			'ENDS_WITH', 'NOT_ENDS_WITH', 'CONTAINS', 'NOT_CONTAINS', 'JOIN', 'TAKE_FIRST', 'TAKE_LAST', 'VALUE'
		];

		$translate(requiredTranslations)
			.then(
			function (translations) {
				$scope.translated.import_success = translations.IMPORT_SUCCESS;
				$scope.translated.code = translations.CODE;
				$scope.translated.name = translations.NAME;
				$scope.translated.description = translations.DESCRIPTION;
				$scope.translated.exists = translations.EXISTS;
				$scope.translated.category = translations.CATEGORY;
				$scope.translated.discontinued = translations.DISCONTINUED;
				$scope.translated.price = translations.PRICE;
				$scope.translated.stock = translations.STOCK;
				$scope.translated.reference = translations.REFERENCE;
				$scope.translated.ean = translations.EAN;
				$scope.translated.trading_price = translations.TRADING_PRICE;
				$scope.translated.units_per_pack = translations.UNITS_PER_PACK;
				$scope.translated.manufacturer = translations.MANUFACTURER;
				$scope.translated.image_url = translations.IMAGE_URL;
				$scope.translated.small_image_url = translations.SMALL_IMAGE_URL;
				$scope.translated.yes = translations.YES;
				$scope.translated.NO = translations.NO;
				$scope.translated.less_than = translations.LESS_THAN;
				$scope.translated.greater_than = translations.GREATER_THAN;
				$scope.translated.saved = translations.SAVED;
				$scope.translated.load_profile_confirm = translations.LOAD_PROFILE_CONFIRM;
				$scope.translated.starts_with = translations.STARTS_WITH;
				$scope.translated.not_starts_with = translations.NOT_STARTS_WITH;
				$scope.translated.ends_with = translations.ENDS_WITH;
				$scope.translated.not_ends_with = translations.NOT_ENDS_WITH;
				$scope.translated.contains = translations.CONTAINS;
				$scope.translated.not_contains = translations.NOT_CONTAINS;
				$scope.translated.join = translations.JOIN;
				$scope.translated.take_first = translations.TAKE_FIRST;
				$scope.translated.take_last = translations.TAKE_LAST;
				$scope.translated.value = translations.VALUE;


				$scope.importGridOptions.columnDefs = [
					{
						field: 'exists', displayName: $scope.translated.exists, enableCellEdit: false,
						filter: { type: uiGridConstants.filter.SELECT, selectOptions: [{ value: 'false', label: 'false' }, { value: 'true', label: 'true' }] }
					},
					{ field: 'reference', displayName: $scope.translated.reference },
					{ field: 'name', displayName: $scope.translated.name },
					{ field: 'description', displayName: $scope.translated.description, visible: false },
					{ field: 'code', displayName: $scope.translated.code, visible: false },
					{ field: 'ean', displayName: $scope.translated.ean, visible: false },
					{ field: 'category', displayName: $scope.translated.category },
					{
						field: 'discontinued', displayName: $scope.translated.discontinued,
						filter: { type: uiGridConstants.filter.SELECT, selectOptions: [{ value: '0', label: 'false' }, { value: '1', label: 'true' }] }
					},
					{
						field: 'price', displayName: $scope.translated.price, visible: false, filters: [
							{
								condition: uiGridConstants.filter.GREATER_THAN,
								placeholder: $scope.translated.greater_than
							},
							{
								condition: uiGridConstants.filter.LESS_THAN,
								placeholder: $scope.translated.less_than
							}
						]
					},
					{
						field: 'stock', displayName: $scope.translated.stock, visible: false, filters: [
							{
								condition: uiGridConstants.filter.GREATER_THAN,
								placeholder: $scope.translated.greater_than
							},
							{
								condition: uiGridConstants.filter.LESS_THAN,
								placeholder: $scope.translated.less_than
							}
						]
					},
					{
						field: 'trading_price', displayName: $scope.translated.trading_price, visible: false, filters: [
							{
								condition: uiGridConstants.filter.GREATER_THAN,
								placeholder: $scope.translated.greater_than
							},
							{
								condition: uiGridConstants.filter.LESS_THAN,
								placeholder: $scope.translated.less_than
							}
						]
					},
					{
						field: 'units_per_pack', displayName: $scope.translated.units_per_pack, visible: false, filters: [
							{
								condition: uiGridConstants.filter.GREATER_THAN,
								placeholder: $scope.translated.greater_than
							},
							{
								condition: uiGridConstants.filter.LESS_THAN,
								placeholder: $scope.translated.less_than
							}
						]
					},
					{ field: 'manufacturer', displayName: $scope.translated.manufacturer, visible: false },
					{ field: 'image_url', displayName: $scope.translated.image_url, visible: false },
					{ field: 'small_image_url', displayName: $scope.translated.small_image_url, visible: false }

				];

				$scope.filterTypes = [
					{ key: 'starts_with', name: $scope.translated.starts_with },
					{ key: 'not_starts_with', name: $scope.translated.not_starts_with },
					{ key: 'ends_with', name: $scope.translated.ends_with },
					{ key: 'not_ends_with', name: $scope.translated.not_ends_with },
					{ key: 'contains', name: $scope.translated.contains },
					{ key: 'not_contains', name: $scope.translated.not_contains }

				];

				$scope.arrayTypes = [
					{ key: 'join', name: $scope.translated.join },
					{ key: 'first', name: $scope.translated.take_first },
					{ key: 'last', name: $scope.translated.take_last }
				];

				$scope.variablesGridOptions.columnDefs = [
					{ field: 'name', displayName: $scope.translated.name },
					{ field: 'value', displayName: $scope.translated.value }
				];

			}
			);

		$scope.busy = false;
		$scope.nodes = {};
		$scope.file = {
			fields_format: {}
		};
		$scope.activeTab = 3;

		$scope.setActiveTab = function (tabIndex) {
			$scope.activeTab = tabIndex;
		}

		$scope.getExcelFields = function (filename) {
			$scope.nodes.sup = $scope.nodes.sup || '';
			FileService.getXmlNodeKeys(filename, $scope.nodes.sup.split(' '))
				.then(
				function (result) {
					$scope.file.fields = result.data;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);
		}

		$scope.uploadFile = function (files) {
			if (files && files.length) {
				$scope.busy = true;
				FileService.upload(files)
					.then(
					function (result) {

						$scope.file.client = files[0].name;
						$scope.file.server = result.data[0];

						$scope.busy = false;

						$scope.getExcelFields($scope.file.server);
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
						$scope.busy = false;
					}
					);

			}
		}

		$scope.importProducts = function () {

			$scope.busy = true;
			ProductService.import($scope.products, $scope.operations.lists.current, $scope.taxes, $scope.discounts, $scope.importSettings)
				.then(
				function (result) {
					MS.showStatus($scope.translated.import_success);
					$scope.busy = false;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
					$scope.busy = false;
				}
				);
		}

		$scope.parseFile = function (file) {
			$scope.busy = true;
			ProductService.parseXml(file.server, file.fields_format, $scope.nodes.sup.split(' '))
				.then(
				function (result) {
					$scope.products = result.data;
					$scope.importGridOptions.data = $scope.products;
					$scope.busy = false;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
					$scope.busy = false;
				}

				);
		}

		$scope.deleteSelected = function () {

			$translate('REMOVE_PRODUCTS', { count: $scope.gridApi.selection.getSelectedCount() })
				.then(
				function (translation) {

					if (!confirm(translation))
						return;

					var selRows = $scope.gridApi.selection.getSelectedRows();

					if (selRows !== null) {
						angular.forEach(selRows, function (value, key) {

							index = $scope.products.indexOf(value);
							$scope.products.splice(index, 1);
							//Deseleccionamos las rows borradas ya que en caso contrario quedan seleccionadas aunque no existan.
							$scope.gridApi.selection.clearSelectedRows();


						});
					}
				}
				);

		}


		/*********************/
		/******PROFILES******/
		/*******************/

		//LOAD PROFILES
		var loadProfiles = function () {
			ProductService.indexImportProfile()
				.then(
				function (result) {
					$scope.profiles = result.data;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5000);
				}
				);
		}

		loadProfiles();

		$scope.saveProfile = function (name, supnodes, formats, operations, settings, taxes, discounts) {
			var profile = {
				name: name,
				supnodes: supnodes,
				name_format: formats.name,
				description_format: formats.description,
				price_format: formats.price,
				stock_format: formats.stock,
				reference_format: formats.reference,
				category_format: formats.category,
				code_format: formats.code,
				ean_format: formats.ean,
				trading_price_format: formats.trading_price,
				units_per_pack_format: formats.units_per_pack,
				manufacturer_format: formats.manufacturer,
				discountinued_format: formats.discontinued,
				image_url_format: formats.image_url,
				small_image_url_format: formats.small_image_url,
				operations: operations,
				import_settings: settings,
				taxes: taxes,
				discounts: discounts
			};



			if ($scope.profile && $scope.profile.name == profile.name) {
				//Update
				profile.id = $scope.profile.id;
				ProductService.updateImportProfile(profile)
					.then(
					function (result) {
						MS.showStatus($scope.translated.saved, 'info');
						loadProfiles();
						loadOperations();
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}
			else {
				//Store
				ProductService.storeImportProfile(profile)
					.then(
					function (result) {
						MS.showStatus($scope.translated.saved, 'info');
						$scope.profiles.push(result.data);
						loadOperations();
					},
					function (error) {
						MS.showStatus(MS.getErrorsString(error.data), 'error');
					}
					);
			}


		}

		$scope.loadProfile = function (profile) {
			if (!profile)
				return;

			if (!confirm($scope.translated.load_profile_confirm))
				return;

			$scope.profile = profile;

			$scope.profileName = profile.name;
			$scope.nodes.sup = profile.supnodes;
			$scope.file.fields_format.name = profile.name_format;
			$scope.file.fields_format.description = profile.description_format;
			$scope.file.fields_format.price = profile.price_format;
			$scope.file.fields_format.stock = profile.stock_format;
			$scope.file.fields_format.reference = profile.reference_format;
			$scope.file.fields_format.category = profile.category_format;
			$scope.file.fields_format.code = profile.code_format;
			$scope.file.fields_format.ean = profile.ean_format;
			$scope.file.fields_format.trading_price = profile.trading_price_format;
			$scope.file.fields_format.units_per_pack = profile.units_per_pack_format;
			$scope.file.fields_format.manufacturer = profile.manufacturer_format;
			$scope.file.fields_format.discontinued = profile.discountinued_format;
			$scope.file.fields_format.image_url = profile.image_url_format;
			$scope.file.fields_format.small_image_url = profile.small_image_url_format;
			$scope.operations.lists.current = profile.operations;
			angular.forEach(profile.operations, function (operation) {
				operation['label'] = $filter('uppercase')(operation['type']);
				operation['opened'] = true;
			});
			$scope.importSettings = profile.settings_array;
			$scope.taxes = profile.taxes;
			$scope.discounts = profile.discounts;

			$scope.activeTab = 0;
		}

		$scope.deleteProfile = function (profile) {
			if (!profile)
				return;

			$translate('DELETE_PROFILE_CONFIRM', { name: profile.name })
				.then(
				function (translation) {

					if (!confirm(translation))
						return;

					ProductService.destroyImportProfile(profile.id)
						.then(
						function (result) {
							MS.showStatus($scope.translated.saved, 'info', -1);
							var index = $scope.profiles.indexOf(profile);
							$scope.profiles.splice(index, 1);
						},
						function (error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error', -1);
						}
						);


				}
				);
		}

		/**************/
		/* OPERATIONS*/
		/************/

		$scope.savedOperationId = 1;

		$scope.operations = {
			selected: null,
			lists: { "current": [/*{label: 'test', type : 'filter', opened : true}*/] },
			templates: [
				{ label: 'FILTER', type: 'filter', opened: true, id: 1, iterations: 1, options: { capsens: false } },
				{ label: 'REPLACE', type: 'replace', opened: true, id: 1, iterations: 1, options: { capsens: false } },
				{ label: 'SUBSTRING', type: 'substring', opened: true, id: 1, iterations: 1, options: { capsens: false, includestart: false, includeend: false } },
				{ label: 'ARRAY', type: 'array', opened: true, id: 1, iterations: 1, options: { godeep: true, separator: ',' } }
			]
		};

		var loadOperations = function () {
			ProductService.indexOperation()
				.then(
				function (result) {
					$scope.operationList = result.data;

					angular.forEach($scope.operationList, function (operation) {
						operation['label'] = $filter('uppercase')(operation['type']);
						operation['opened'] = true;
					});
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5);
				}
				);
		}

		loadOperations();


		$scope.startChange = function (item) {
			item.options.startIsNumber = !isNaN(parseInt(item.options.start));
		}

		$scope.endChange = function (item) {
			item.options.endIsNumber = !isNaN(parseInt(item.options.end));
		}

		$scope.deleteOperation = function (operation) {
			ProductService.destroyOperation(operation.id)
				.then(
				function (result) {
					loadOperations();
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error', 5);
				}
				);
		}


		/********************/
		/*TAXES n DISCOUNTS*/
		/******************/

		$scope.discounts = [];
		$scope.taxes = [];

		//Tax select
		ProductService.indexTax()
			.then(
			function (result) {
				$scope.taxList = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
			);

		//Discount select
		ProductService.indexDiscount()
			.then(
			function (result) {
				$scope.discountList = result.data;
			},
			function (error) {
				MS.showStatus(MS.getErrorsString(error.data), 'error');
			}
			);



		/*********************/
		/* IMPORT VARIABLES */
		/*******************/

		$scope.loadVariables = function () {
			ProductService.indexImportVariable()
				.then(
				function (result) {
					$scope.importVariables = result.data;
					$scope.variablesGridOptions.data = $scope.importVariables;
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);
		}

		$scope.loadVariables();

		$scope.createVariable = function () {
			$scope.importVariables.push({});
		}

		$scope.saveVariable = function (variable) {

			ProductService.storeImportVariable(variable)
				.then(
				function (result) {
					MS.showStatus('SAVED', 'info');
				},
				function (error) {
					MS.showStatus(MS.getErrorsString(error.data), 'error');
				}
				);
		}

		$scope.deleteSelectedVariables = function () {

			if (!confirm($translate.instant('REMOVE_VARIABLES', { count: $scope.variablesGridApi.selection.getSelectedCount() })))
				return;

			angular.forEach($scope.variablesGridApi.selection.getSelectedRows(), function (row) {
				if (row.id)
					ProductService.destroyImportVariable(row.id)
						.then(
						function (result) {
							MS.showStatus('REMOVED', 'info');
							$scope.loadVariables();
						},
						function (error) {
							MS.showStatus(MS.getErrorsString(error.data), 'error');
						}
						);
			});


		}

	}]);