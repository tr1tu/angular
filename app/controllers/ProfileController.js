app.controller('ProfileController', ['$scope', '$state', 'UserService', 'AuthService', 'MessagesService', '$timeout',
	function ($scope, $state, UserService, AuthService, MS, $timeout) {

		$scope.currentUser = AuthService.getUser();


		$scope.updateUserProfile = function () {

			UserService.updateSelf($scope.currentUser)
				.then(
				function (result) {
					var user = JSON.stringify(result.data);

					// Set the stringified user data into local storage
					localStorage.setItem('user', user);

					MS.clearAlerts();
					MS.addAlert('SAVED', 'info', 5000);
					$state.go('home');

					
					
				},
				function (error) {
					MS.addAlert(MS.getErrorsString(error.data), 'error');
				});

		}


	}]);